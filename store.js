import { configureStore } from "@reduxjs/toolkit";
import modalReducer from "./features/modal/modalSlice";
import startupReducer from "./features/startups/startupSlice";
import diasporaReducer from "./features/diaspora/diasporaSlice";
import feedReducer from "./features/feeds/feedSlice";
import authReducer from "./features/auth/authSlice";
import thunk from "redux-thunk";

export const store = configureStore({
  reducer: {
    modal: modalReducer,
    startup: startupReducer,
    diaspora: diasporaReducer,
    feed: feedReducer,
    auth: authReducer,
  },
  devTools: process.env.NODE_ENV !== "production",
  middleware: [thunk],
});

export const getLanguageName = (shortName) => {
  switch (shortName) {
    case "en":
      return "English";
    case "fr":
      return "French";
    default:
      return null;
  }
};

export const standardDateFormat = (arg) => {
  const date = new Date(arg);
  const yyyy = date.getFullYear();
  let mm = date.getMonth() + 1;
  let dd = date.getDay();

  if (dd < 10) dd = "0" + dd;
  if (mm < 10) mm = "0" + mm;

  const result = yyyy + "-" + mm + "-" + dd;
  return result;
};

module.exports = {
  images: {
    domains: ["res.cloudinary.com", "eu.ui-avatars.com"],
  },
  reactStrictMode: true,
};

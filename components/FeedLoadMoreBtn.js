import { useDispatch, useSelector } from "react-redux";
import { loadMoreRecords } from "../features/feeds/feedSlice";

const FeedLoadMoreBtn = ({ language }) => {
  const dispatch = useDispatch();
  const loadMoreBtn = useSelector((state) => state.feed.visibleLoadMoreBtn);
  const page = useSelector((state) => state.feed.currentPage);

  return (
    loadMoreBtn && (
      <button
        className="w-full"
        onClick={() => {
          let payload = {
            page,
            language,
          };
          dispatch(loadMoreRecords(payload));
        }}
      >
        <div className="shadow-sm border-2 border-slate-300 p-4 text-green-600 my-5 text-center bg-white font-semibold">
          Load More
        </div>
      </button>
    )
  );
};
export default FeedLoadMoreBtn;

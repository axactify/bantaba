import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { close } from "../features/modal/modalSlice";
import { rejectStartup } from "../features/startups/startupSlice";

const RejectStartup = () => {
  const [reason, setReason] = useState("");
  const dispatch = useDispatch();
  const data = useSelector((state) => state.modal.data);
  const startup = useSelector((state) => state.startup.selectedStartup);
  const loading = useSelector((state) => state.startup.loading);
  const [email, setEmail] = useState("");

  if (loading) {
    return <></>;
  }

  return (
    <>
      <div className="shadow-md rounded-3xl bg-white mb-20">
        <div className="bg-red-700 text-white p-6 shadow-2xl rounded-tl-3xl rounded-tr-3xl">
          <div className="flex justify-between items-center">
            <span className="text-2xl font-semibold ml-8">
              Rejecting Startup: {data.startup_name}
            </span>
            <img
              src="/img/Group 35824.png"
              width="50"
              height="50"
              className="cursor-pointer"
              onClick={() => dispatch(close("RejectStartup"))}
            />
          </div>
        </div>
        {/* section 1 */}
        <div className="p-5 bg-white">
          <div className="mb-4">
            <p>
              Email <span className="text-red-600">*</span>
            </p>
            <input
              type="text"
              className="w-full border-2 rounded-sm outline-none p-2 border-slate-500 mt-3"
              defaultValue={data.email}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>

          <div className="mt-6 mb-6 border-2 border-orange-500 bg-orange-200 text-center px-3 py-2 rounded-md text-lg">
            <p>
              The User Language is English. Make sure to send your message in
              English. Thanks !!
            </p>
          </div>

          <p>
            Reason <span className="text-red-600">*</span>
          </p>
          <textarea
            className="w-full border-2 rounded-sm outline-none p-2 border-slate-500 mt-3"
            rows="6"
            spellCheck="false"
            onChange={(e) => setReason(e.target.value)}
          />

          <div className="mt-8 mb-4 text-center cursor-pointer">
            <button
              onClick={() => {
                let payload = {
                  id: data.id,
                  email,
                  link: startup.overview.website || "https://ourbantaba.com",
                  reason: reason,
                };
                dispatch(rejectStartup(payload));
                dispatch(close("RejectStartup"));
              }}
              className="rounded-3xl border-2 text-green-500 text-lg px-16 py-2 font-semibold bg-green-50 hover:bg-green-100"
            >
              Send
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default RejectStartup;

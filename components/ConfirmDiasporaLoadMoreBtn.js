import { useDispatch, useSelector } from "react-redux";
import {
  fetchDiasporaDateRange,
  fetchLastMonthRecords,
  loadMoreRecords,
} from "../features/diaspora/diasporaSlice";

const ConfirmDiasporaLoadMoreBtn = ({ type }) => {
  const dispatch = useDispatch();
  const applyFilter = useSelector((state) => state.diaspora.applyFilter);
  const visibleLoadMoreBtn = useSelector(
    (state) => state.diaspora.visibleLoadMoreBtn
  );
  const currentPage = useSelector((state) => state.diaspora.currentPage);
  const filterCurrentPage = useSelector(
    (state) => state.diaspora.filterCurrentPage
  );
  const actionFilter = useSelector((state) => state.diaspora.actionFilter);
  const startDate = useSelector((state) => state.diaspora.startDate);
  const endDate = useSelector((state) => state.diaspora.endDate);

  if (
    visibleLoadMoreBtn &&
    applyFilter &&
    actionFilter === "fetchLastMonthRecords"
  ) {
    return (
      <button className="w-full">
        <div
          onClick={() => {
            dispatch(fetchLastMonthRecords(filterCurrentPage));
          }}
          className="shadow-sm border-2 border-slate-300 p-4 text-green-600 my-5 text-center bg-white font-semibold"
        >
          Load More
        </div>
      </button>
    );
  }

  if (
    visibleLoadMoreBtn &&
    applyFilter &&
    actionFilter === "fetchDiasporaDateRange"
  ) {
    return (
      <button
        className="w-full"
        onClick={() => {
          const payload = {
            page: filterCurrentPage,
            startDate,
            endDate,
          };
          dispatch(fetchDiasporaDateRange(payload));
        }}
      >
        <div className="shadow-sm border-2 border-slate-300 p-4 text-green-600 my-5 text-center bg-white font-semibold">
          Load More
        </div>
      </button>
    );
  }

  return (
    visibleLoadMoreBtn && (
      <button
        className="w-full"
        onClick={() => {
          let payload = {
            page: currentPage + 1,
            type,
          };
          dispatch(loadMoreRecords(payload));
        }}
      >
        <div className="shadow-sm border-2 border-slate-300 p-4 text-green-600 my-5 text-center bg-white font-semibold">
          Load More
        </div>
      </button>
    )
  );
};
export default ConfirmDiasporaLoadMoreBtn;

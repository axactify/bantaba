import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { close } from "../features/modal/modalSlice";

const ViewDiasPora = () => {
  const dispatch = useDispatch();
  const diaspora = useSelector((state) => state.diaspora.selectedDiaspora);
  const loading = useSelector((state) => state.diaspora.loading);

  if (loading) {
    return <></>;
  }

  return (
    <>
      <div className="shadow-md rounded-3xl bg-white mb-5 overflow-y-auto z-50">
        <div className="bg-green-600 text-white p-8 shadow-lg rounded-tl-3xl rounded-tr-0xl">
          <div className="flex justify-between items-center">
            <span className="text-3xl font-semibold ml-8">
              Viewing Diaspora: {diaspora.diaspora_title}
            </span>
            <img
              src="/img/Group 35823.png"
              width="50"
              height="50"
              className="cursor-pointer"
              onClick={() => dispatch(close("ViewStartup"))}
            />
          </div>
        </div>
        {/* section 1 */}
        <div className="p-5 bg-white">
          <div className="grid grid-cols-2 gap-10">
            <div className="shadow-xl">
              <div
                className="rounded-tl-xl rounded-tr-xl px-8 py-2"
                style={{ backgroundColor: "#CCE8D2" }}
              >
                <p className="text-lg">About Us</p>
              </div>
              <div className="bg-white px-8 py-4">{diaspora.about}</div>
            </div>
            <div className="shadow-xl">
              <div
                className="rounded-tl-xl rounded-tr-xl px-8 py-2"
                style={{ backgroundColor: "#CCE8D2" }}
              >
                <p className="text-lg">Overview</p>
              </div>
              <div className="bg-white px-8 py-4">
                <div className="flex justify-between">
                  <p>Lives In</p>
                  <p>{diaspora.overview.LivesIn}</p>
                </div>
                <div className="flex justify-between">
                  <p>Origin</p>
                  <p>{diaspora.overview.Origin}</p>
                </div>
                <div className="flex justify-between">
                  <p>Education Level</p>
                  <p>{diaspora.overview.EducationLevel}</p>
                </div>
                <div className="flex justify-between">
                  <p>Years of experience</p>
                  <p>{diaspora.overview.YearsOfExperience}</p>
                </div>
                <div className="flex justify-between">
                  <p>Avatar</p>
                  <p className="text-green-500 font-semibold">
                    <a href={diaspora.overview.Avatar}>View Avatar</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* section 2 */}
          <div className="shadow-xl mt-10 bg-white">
            <div className="grid grid-cols-2">
              <div>
                <div
                  className="rounded-tl-xl px-8 py-2"
                  style={{ backgroundColor: "#CCE8D2" }}
                >
                  <p>Looking To</p>
                </div>
                <div className="px-8 py-4">
                  {diaspora.LookingFor.map((x) => (
                    <p key={x.id}>{x.typeOfSupport}</p>
                  ))}
                </div>
              </div>
              <div>
                <div
                  className="rounded-tr-xl px-8 py-2"
                  style={{ backgroundColor: "#CCE8D2" }}
                >
                  <p>Areas of expertise</p>
                </div>
                <div className="px-8 py-4">
                  {diaspora.AreasOfExpertise.map((x) => (
                    <p key={x.id}>{x.name}</p>
                  ))}
                </div>
              </div>
            </div>
          </div>
          {/* section 3 */}
          <div className="shadow-xl mt-10 bg-white">
            <div
              className="rounded-tl-xl px-8 py-2"
              style={{ backgroundColor: "#CCE8D2" }}
            >
              <p>Work Experience</p>
            </div>
            <div className="grid grid-cols-6 mb-4">
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Title</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Company</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Location</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>From-To</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Type</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Description</p>
                </div>
              </div>
            </div>

            {diaspora.workExperience.length > 0 ? (
              diaspora.workExperience.map((x) => (
                <div key={x.id} className="grid grid-cols-6 mb-4">
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>{x.title}</p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>{x.company}</p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>
                        {x.city}, {x.country}
                      </p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>
                        {x.startDate} - {x.endDate}
                      </p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>{x.type}</p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>{x.description}</p>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <p className="text-center py-2">
                There is no work experience for this diaspora
              </p>
            )}
          </div>
          {/* end section 4 */}
          {/* section 5 */}
          <div className="shadow-xl mt-10 bg-white">
            <div
              className="rounded-tl-xl px-8 py-2"
              style={{ backgroundColor: "#CCE8D2" }}
            >
              <p>Education</p>
            </div>
            <div className="grid grid-cols-6 mb-4">
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>University</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Field</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Location</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>From-To</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Degree</p>
                </div>
              </div>
              <div className="bg-slate-100">
                <div className="px-8 py-2">
                  <p>Description</p>
                </div>
              </div>
            </div>
            {diaspora.educations.length > 0 ? (
              diaspora.educations.map((x) => (
                <div key={x.id} className="grid grid-cols-6 mb-4">
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>{x.university}</p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p></p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>
                        {x.city} - {x.country}
                      </p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>
                        {x.startDate} - {x.endDate}
                      </p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>{x.level}</p>
                    </div>
                  </div>
                  <div className="bg-slate-100">
                    <div className="px-8 py-2">
                      <p>{x.description}</p>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <p className="text-center py-2">
                There is no educations for this diaspora
              </p>
            )}
          </div>
          {/* end section 4 */}
          <div className="mt-8 text-center cursor-pointer">
            <button
              onClick={() => dispatch(close("ViewStartup"))}
              className="rounded-3xl border-2 text-green-500 text-xl px-8 py-2 font-semibold"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewDiasPora;

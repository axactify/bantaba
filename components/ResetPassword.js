import { useState } from "react";
import { useDispatch } from "react-redux";
import { resetPassword } from "../features/auth/authSlice";
import { useRouter } from "next/router";

export default function ResetPassword() {
  const dispatch = useDispatch();
  const router = useRouter();
  const [inputTypeField1, setInputTypeField1] = useState(false);
  const [inputTypeField2, setInputTypeField2] = useState(false);
  const [password, setPassword] = useState("");
  const [confirmedPassword, setConfirmedPassword] = useState("");

  const submitForm = (e) => {
    e.preventDefault();
    if (password !== confirmedPassword) {
      alert("Password is not matched");
    } else {
      const payload = {
        password,
        password_confirm: confirmedPassword,
      };
      dispatch(resetPassword(payload));
      router.push("/login");
    }
  };

  return (
    <>
      <div className="form bg-white">
        <form onSubmit={submitForm} className="w-64 mx-auto">
          <p className="text-center mt-10 mb-7 text-base">
            For the safety your account please use strong password.
          </p>
          <img src="/img/Group 16058.png" className="mx-auto h-20 mb-10" />
          <div className="flex justify-between">
            <p className="text-sm">New Password</p>
            <img
              src="img/Group 16063.png"
              className="w-6 h-4"
              onClick={() => setInputTypeField1(!inputTypeField1)}
            />
          </div>
          <input
            type={inputTypeField1 ? "text" : "password"}
            className="border-b-2 border-slate-500 outline-0 w-full h-7"
            onChange={(e) => setPassword(e.target.value)}
            minLength={3}
            maxLength={20}
            required
          />

          <div className="flex justify-between mt-5">
            <p className="text-sm">Re-type Password</p>
            <img
              src="img/Group 16063.png"
              className="w-6 h-4"
              onClick={() => setInputTypeField2(!inputTypeField2)}
            />
          </div>
          <input
            type={inputTypeField2 ? "text" : "password"}
            className="border-b-2 border-slate-500 outline-0 w-full h-7"
            onChange={(e) => setConfirmedPassword(e.target.value)}
            minLength={3}
            maxLength={20}
            required
          />

          <div className="text-center">
            <button
              type="submit"
              className="px-8 py-2 font-semibold bg-green-600 hover:bg-green-700 text-white mt-8 rounded-md"
            >
              Reset Password
            </button>
          </div>
        </form>
      </div>
    </>
  );
}

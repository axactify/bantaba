import { useDispatch, useSelector } from "react-redux";
import { close } from "../features/modal/modalSlice";
import { deleteRecord } from "../features/startups/startupSlice";

const ConfirmDeletionStartup = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.modal.data);

  return (
    <>
      <div className="shadow-md rounded-3xl bg-white">
        <div className="bg-red-700 text-white p-4 shadow-2xl rounded-tl-3xl rounded-tr-3xl">
          <div className="flex justify-between items-center">
            <span></span>
            <span className="text-2xl font-semibold text-center">
              Confirm Deletion
            </span>
            <div>
              <img
                src="/img/Group 35824.png"
                width="50"
                height="50"
                className="cursor-pointer"
                onClick={() => dispatch(close("ConfirmDeletionStartup"))}
              />
            </div>
          </div>
        </div>
        {/* section 1 */}
        <div className="p-14 py-10 bg-white">
          <div className="p-6 border-l-8 border-red-600 bg-red-100">
            <div className="flex">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="41.578"
                height="20.376"
                viewBox="0 0 41.578 38.376"
              >
                <path
                  id="Icon_ionic-ios-warning"
                  data-name="Icon ionic-ios-warning"
                  d="M21.454,6.089l-17.7,32.3a3.038,3.038,0,0,0,2.708,4.487H41.871a3.044,3.044,0,0,0,2.708-4.487L26.87,6.089A3.1,3.1,0,0,0,21.454,6.089Zm4.467,13.4-.36,12.192h-2.8L22.4,19.491ZM24.162,38.319a1.84,1.84,0,1,1,1.909-1.839A1.857,1.857,0,0,1,24.162,38.319Z"
                  transform="translate(-3.375 -4.5)"
                  fill="#f35627"
                />
              </svg>
              <p className="ml-4">
                Are you sure you want to delete ? You can&apos;t undo this
                action.
              </p>
            </div>
          </div>

          <div className="flex items-center justify-center">
            <div className="mt-8 text-center cursor-pointer">
              <button
                onClick={() => dispatch(close("ViewStartup"))}
                className="rounded-full mr-20 border-2 text-white text-sm xl:text-1xl px-6 py-3 font-semibold bg-slate-500 hover:bg-slate-600"
              >
                Cancel
              </button>
            </div>

            <div className="mt-8 text-center cursor-pointer">
              <button
                onClick={() => {
                  dispatch(deleteRecord(data.id));
                  dispatch(close("ViewStartup"));
                }}
                className="flex items-center rounded-full border-2 text-white text-sm xl:text-1xl px-4 py-3 font-semibold bg-red-600 hover:bg-red-800"
              >
                <span> Delete Media</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ConfirmDeletionStartup;

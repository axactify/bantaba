import React from "react";
import { useSelector } from "react-redux";
import Link from "next/link";

function Header({ user }) {
  let isModalOpen = useSelector((state) => state.modal.open);
  let classes = "flex justify-start items-center sticky top-0 w-full shadow ";
  classes += isModalOpen ? "z-0" : "z-10";

  return (
    <div className={classes} style={{ backgroundColor: "#F8F8FB" }}>
      <div className="w-28">
        <Link href="/dashboard">
          <a>
            <img src="/logo.svg" className="mx-auto w-16" />
          </a>
        </Link>
      </div>
      <div className="bg-white flex justify-between items-center w-full p-2  shadow-sm">
        <div className="flex items-center">
          <img
            src="/img/burger.png"
            width="19"
            height="10"
            className="mr-5 ml-5"
          />

          <p className="text-sm md:text-lg xl:text-2xl text-slate-500 poppines font-bold">
            Admin Dashboard
          </p>

          <div className="searchbox ml-10 hidden xl:block">
            <div className="relative text-gray-600 focus-within:text-gray-400 border-b-2 border-gray-300">
              <span className="absolute inset-y-0 left-0 flex items-center">
                <button
                  type="submit"
                  className="px-1 focus:outline-none focus:shadow-outline"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="13.449"
                    height="13.449"
                    viewBox="0 0 16.449 16.449"
                  >
                    <g
                      id="Icon_feather-search"
                      data-name="Icon feather-search"
                      transform="translate(-3.9 -3.9)"
                    >
                      <path
                        id="Path_9243"
                        data-name="Path 9243"
                        d="M17.833,11.167A6.667,6.667,0,1,1,11.167,4.5,6.667,6.667,0,0,1,17.833,11.167Z"
                        fill="none"
                        stroke="#495172"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="1.2"
                      />
                      <path
                        id="Path_9244"
                        data-name="Path 9244"
                        d="M28.6,28.6l-3.625-3.625"
                        transform="translate(-9.1 -9.1)"
                        fill="none"
                        stroke="#495172"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="1.2"
                      />
                    </g>
                  </svg>
                </button>
              </span>
              <input
                type="search"
                name="q"
                className="px-2 pb-1 text-sm text-slate-800 placeholder-gray-600 w-80 rounded-md pl-8 focus:outline-none focus:bg-white focus:text-gray-900"
                placeholder="Search"
                autoComplete="off"
              />
            </div>
          </div>
        </div>
        <div className="flex justify-end items-center">
          <img src="/img/Ellipse 2.png" className="w-14 h-14 cursor-pointer" />
          <p className="ml-1 text-slate-300">{user.name}</p>
        </div>
      </div>
    </div>
  );
}

export default Header;

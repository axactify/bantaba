import { useDispatch, useSelector } from "react-redux";
import {
  fetchLastMonthRecords,
  fetchStartupDateRange,
  loadMoreRecords,
} from "../features/startups/startupSlice";

const ConfirmStartupLoadMoreBtn = ({ type }) => {
  const dispatch = useDispatch();
  const applyFilter = useSelector((state) => state.startup.applyFilter);
  const visibleLoadMoreBtn = useSelector(
    (state) => state.startup.visibleLoadMoreBtn
  );
  const currentPage = useSelector((state) => state.startup.currentPage);
  const filterCurrentPage = useSelector(
    (state) => state.startup.filterCurrentPage
  );
  const actionFilter = useSelector((state) => state.startup.actionFilter);
  const startDate = useSelector((state) => state.startup.startDate);
  const endDate = useSelector((state) => state.startup.endDate);

  if (
    visibleLoadMoreBtn &&
    applyFilter &&
    actionFilter === "fetchLastMonthRecords"
  ) {
    return (
      <button className="w-full">
        <div
          onClick={() => {
            dispatch(fetchLastMonthRecords(filterCurrentPage));
          }}
          className="shadow-sm border-2 border-slate-300 p-4 text-green-600 my-5 text-center bg-white font-semibold"
        >
          Load More
        </div>
      </button>
    );
  }

  if (
    visibleLoadMoreBtn &&
    applyFilter &&
    actionFilter === "fetchStartupDateRange"
  ) {
    return (
      <button
        className="w-full"
        onClick={() => {
          const payload = {
            page: filterCurrentPage,
            startDate,
            endDate,
          };
          dispatch(fetchStartupDateRange(payload));
        }}
      >
        <div className="shadow-sm border-2 border-slate-300 p-4 text-green-600 my-5 text-center bg-white font-semibold">
          Load More
        </div>
      </button>
    );
  }

  return (
    visibleLoadMoreBtn && (
      <button
        className="w-full"
        onClick={() => {
          let payload = {
            page: currentPage + 1,
            type,
          };
          dispatch(loadMoreRecords(payload));
        }}
      >
        <div className="shadow-sm border-2 border-slate-300 p-4 text-green-600 my-5 text-center bg-white font-semibold">
          Load More
        </div>
      </button>
    )
  );
};
export default ConfirmStartupLoadMoreBtn;

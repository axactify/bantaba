import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { close } from "../features/modal/modalSlice";
import Image from "next/image";
import React from "react";

const ViewStartup = () => {
  const dispatch = useDispatch();
  const startup = useSelector((state) => state.startup.selectedStartup);
  const loading = useSelector((state) => state.startup.loading);

  if (loading) {
    return <></>;
  }

  const d = new Date(startup.overview.FoundedIn);
  let FoundedIn = d.getFullYear();

  return (
    <div className="shadow-md rounded-3xl bg-white mb-5 overflow-y-auto z-50">
      <div className="bg-green-600 text-white p-4 xl:p-6 shadow-lg rounded-tl-3xl rounded-tr-0xl">
        <div className="flex justify-between items-center">
          <span className="text-2xl xl:text-3xl font-semibold ml-8">
            Viewing Startup: {startup.startup_name}
          </span>
          <img
            src="/img/Group 35823.png"
            onClick={() => dispatch(close("ViewStartup"))}
            width="50"
            height="50"
            className="cursor-pointer"
          />
        </div>
      </div>
      {/* section 1 */}
      <div className="p-5 bg-white">
        <div className="grid grid-cols-1 xl:grid-cols-2  gap-10">
          <div className="shadow-xl">
            <div
              className="rounded-tl-xl rounded-tr-xl px-8 py-2"
              style={{ backgroundColor: "#CCE8D2" }}
            >
              <p className="text-lg">About Us</p>
            </div>
            <div className="bg-white px-8 py-4">{startup.about}</div>
          </div>
          <div className="shadow-xl">
            <div
              className="rounded-tl-xl rounded-tr-xl px-8 py-2"
              style={{ backgroundColor: "#CCE8D2" }}
            >
              <p className="text-lg">Overview</p>
            </div>
            <div className="bg-white px-8 py-4">
              <div className="flex justify-between">
                <p>Industry</p>
                <p>{startup.overview.Industry}</p>
              </div>
              <div className="flex justify-between">
                <p>Employees</p>
                <p>{startup.overview.Employees}</p>
              </div>
              <div className="flex justify-between">
                <p>Headquarter</p>
                <p>{startup.overview.Headquarter}</p>
              </div>
              <div className="flex justify-between">
                <p>Founded in</p>
                <p>{FoundedIn}</p>
              </div>
              <div className="flex justify-between">
                <p>Stage</p>
                <p>{startup.overview.Stage}</p>
              </div>
              <div className="flex justify-between">
                <p>Website</p>
                <p>{startup.overview.Website || "-"}</p>
              </div>
              <div className="flex justify-between">
                <p>Seeking funding</p>
                <p>{startup.overview.SeekingFunding}</p>
              </div>
              <div className="flex justify-between">
                <p>Followers</p>
                <p>{startup.overview.Followers}</p>
              </div>
              <div className="flex justify-between">
                <p>View</p>
                <p className="text-green-500 font-semibold">
                  <Link href={`${startup.overview.Logo}`}>
                    <a>View Logo</a>
                  </Link>
                </p>
              </div>
              <div className="flex justify-between">
                <p>Video</p>
                <p className="text-green-500 font-semibold">
                  <Link href={`${startup.overview.Video}`}>
                    <a>View Video</a>
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
        {/* section 2 */}
        <div className="shadow-xl mt-10 bg-white">
          <div className="grid grid-cols-2">
            <div>
              <div
                className="rounded-tl-xl px-8 py-2"
                style={{ backgroundColor: "#CCE8D2" }}
              >
                <p>Looking To</p>
              </div>
              <div className="px-8 py-4">
                {startup.LookingFor.map((x) => (
                  <p key={x.id}>{x.typeOfSupport}</p>
                ))}
              </div>
            </div>
            <div>
              <div
                className="rounded-tr-xl px-8 py-2"
                style={{ backgroundColor: "#CCE8D2" }}
              >
                <p>Within</p>
              </div>
              <div className="px-8 py-4">
                {startup.Within.map((x) => (
                  <p key={x.id}>{x.name}</p>
                ))}
              </div>
            </div>
          </div>
        </div>
        {/* section 3 */}
        <div className="shadow-xl mt-10 bg-white">
          <div
            className="rounded-tl-xl px-8 py-2"
            style={{ backgroundColor: "#CCE8D2" }}
          >
            <p>Team</p>
          </div>

          {startup.team.length > 0 ? (
            startup.team.map((t) => (
              <div key={t.id} className="grid grid-cols-4 p-2">
                <div className="bg-slate-100">
                  <div className="px-8 py-2 flex items-center">
                    <Image src={t.image} width="30" height="30" />
                    <p className="ml-7">{t.name}</p>
                  </div>
                </div>
                <div className="bg-slate-100">
                  <div className="px-8 py-2">
                    <p>{t.position}</p>
                  </div>
                </div>
                <div className="bg-slate-100">
                  <div className="px-8 py-2">
                    <p>{t.updated_at}</p>
                  </div>
                </div>
                <div className="bg-slate-100">
                  <div className="px-8 py-2">
                    <p>{t.description}</p>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <p className="text-center py-2">
              There is no team for this startup
            </p>
          )}
        </div>
        {/* end section 3 */}
        <div className="mt-8 text-center">
          <button
            onClick={() => dispatch(close("ViewStartup"))}
            className="rounded-3xl border-2 text-green-500 text-xl px-8 py-2 font-semibold"
          >
            Close
          </button>
        </div>
      </div>
    </div>
  );
};

export default ViewStartup;

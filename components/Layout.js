import React from "react";
import Header from "./Header";
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";
import { useSelector } from "react-redux";
function Layout({ children, user }) {
  const router = useRouter();
  let isModalOpen = useSelector((state) => state.modal.open);
  let classes = "w-28 bg-white max-h-max fixed left-0 h-full shadow-md ";
  classes += isModalOpen ? "z-0" : "z-10";

  return (
    <div>
      <Header user={user} />
      <div className="flex">
        <div className={classes}>
          <ul className="flex flex-col space-y-1 space-x-1  text-sm text-center text-slate-400 sidebar h-2/3 overflow-y-scroll">
            <li
              className={`rounded-md py-2 ml-1 mt-1 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/dashboard"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 32.95 32.95"
                    >
                      <g id="metrics" transform="translate(1.5 1.5)">
                        <circle
                          id="Ellipse_1"
                          data-name="Ellipse 1"
                          cx="5.613"
                          cy="5.613"
                          r="5.613"
                          transform="translate(0 18.725)"
                          fill="#b5bad0"
                          stroke="#f3f4f7"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="3"
                        />
                        <path
                          id="Path_13"
                          data-name="Path 13"
                          d="M2.5,8.668V.5A24.5,24.5,0,0,1,27,25H18.837"
                          transform="translate(2.946 -0.5)"
                          fill="#b5bad0"
                        />
                        <line
                          id="Line_6"
                          data-name="Line 6"
                          y1="7.041"
                          x2="7.041"
                          transform="translate(9.997 12.912)"
                          fill="none"
                          stroke="#f3f4f7"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="3"
                        />
                      </g>
                    </svg>
                  </div>
                  Dashboad
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/confirmed-diaspora" ||
                router.pathname === "/non-confirmed-diaspora"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/confirmed-diaspora">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      id="Group_15918"
                      data-name="Group 15918"
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 42.948 49.148"
                    >
                      <path
                        id="Path_10391"
                        data-name="Path 10391"
                        d="M1647.1,1073.819h-1.235a3.418,3.418,0,0,0-3.589,3.591c0,.253,0,.506,0,.8-.752-.1-1.47-.192-2.185-.3a32.7,32.7,0,0,1-6.155-1.423,15.682,15.682,0,0,1-1.983-.889,4.594,4.594,0,0,1-.975-.748,1.385,1.385,0,0,1-.434-1.227c.073-.652.047-1.319.161-1.962a5.088,5.088,0,0,1,3.014-3.784,20.358,20.358,0,0,1,4.224-1.343c1.905-.464,3.812-.928,5.71-1.422a9.322,9.322,0,0,0,1.392-.552,2.527,2.527,0,0,0,1.523-2.7,2.8,2.8,0,0,0-.605-1.563,12.849,12.849,0,0,1-1.928-3.884c-.143-.478-.209-.978-.331-1.463a.772.772,0,0,0-.217-.317,5.334,5.334,0,0,1-.536-.5,6.469,6.469,0,0,1-1.253-4.884,4.775,4.775,0,0,1,.313-.815,1.173,1.173,0,0,0,.061-.445,12.326,12.326,0,0,1,.771-5.406,8.236,8.236,0,0,1,3.27-3.8,10.62,10.62,0,0,1,13.864,1.824,8.162,8.162,0,0,1,1.811,4.19,12.724,12.724,0,0,1,.139,3.345.37.37,0,0,0,.027.224,3.272,3.272,0,0,1,.414,2.325,6.852,6.852,0,0,1-.886,2.844,3.34,3.34,0,0,1-1,1.139.661.661,0,0,0-.2.423,12.046,12.046,0,0,1-1.746,4.5c-.223.37-.492.71-.723,1.075a2.744,2.744,0,0,0,.885,3.732,10.553,10.553,0,0,0,2.92,1.067c2.171.535,4.349,1.045,6.516,1.6a9.8,9.8,0,0,1,2.915,1.235,5.044,5.044,0,0,1,2.359,3.916c.057.546.063,1.1.08,1.646a1.174,1.174,0,0,1-.412.945,7.778,7.778,0,0,1-1.3.951,16.957,16.957,0,0,1-4.243,1.465,49.764,49.764,0,0,1-5.793.977c0-.343.011-.672,0-1a3.369,3.369,0,0,0-1.912-3.026,3.214,3.214,0,0,0-1.563-.352c-.439,0-.878,0-1.346,0v-1.3A3.412,3.412,0,0,0,1653.4,1069c-.953,0-1.905,0-2.858,0a3.411,3.411,0,0,0-3.443,3.438C1647.1,1072.878,1647.1,1073.32,1647.1,1073.819Z"
                        transform="translate(-1630.53 -1037)"
                        fill={`${
                          router.pathname === "/confirmed-diaspora" ||
                          router.pathname === "/non-confirmed-diaspora"
                            ? "#FFFFFF"
                            : "#b5bad0"
                        }`}
                      />
                      <path
                        id="Path_10392"
                        data-name="Path 10392"
                        d="M1660.075,1107.364h-.348q-1.621,0-3.241,0a1.127,1.127,0,0,1-1.234-1.222q0-1.37,0-2.74a1.119,1.119,0,0,1,1.222-1.228q1.621,0,3.241,0h.359v-.335c0-1.09,0-2.181,0-3.271a1.117,1.117,0,0,1,1.2-1.218q1.4-.005,2.8,0a1.113,1.113,0,0,1,1.188,1.2c0,1.09,0,2.181,0,3.271v.349h.346q1.635,0,3.271,0a1.116,1.116,0,0,1,1.207,1.214q0,1.385,0,2.769a1.122,1.122,0,0,1-1.218,1.207c-1.187,0-2.374,0-3.605,0v.329q0,1.635,0,3.271a1.121,1.121,0,0,1-1.2,1.222q-1.4.01-2.8,0a1.116,1.116,0,0,1-1.189-1.206C1660.073,1109.791,1660.075,1108.6,1660.075,1107.364Z"
                        transform="translate(-1641.198 -1063.042)"
                        fill={`${
                          router.pathname === "/confirmed-diaspora" ||
                          router.pathname === "/non-confirmed-diaspora"
                            ? "#FFFFFF"
                            : "#b5bad0"
                        }`}
                      />
                    </svg>
                  </div>
                  Diaspora
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/confirmed-startups" ||
                router.pathname === "/non-confirmed-startups"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/confirmed-startups">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      id="Group_35756"
                      data-name="Group 35756"
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 31.995 50.562"
                    >
                      <path
                        id="Path_37733"
                        data-name="Path 37733"
                        d="M725.622,747.824a6.976,6.976,0,0,1,.682.466,25.178,25.178,0,0,1,9,21.057,34.6,34.6,0,0,1-3.355,12.894.9.9,0,0,1-.912.573q-5.494-.012-10.987,0a.914.914,0,0,1-.927-.589,35.462,35.462,0,0,1-3.043-9.876,26.7,26.7,0,0,1,1.332-13.795,25.893,25.893,0,0,1,7.646-10.507,2.8,2.8,0,0,1,.359-.223Zm-.041,9.738a4.247,4.247,0,1,0,4.2,4.351A4.249,4.249,0,0,0,725.581,757.562Zm2.29,14.507a2.332,2.332,0,1,0-2.32,2.345A2.328,2.328,0,0,0,727.87,772.069Z"
                        transform="translate(-709.531 -747.824)"
                        fill={`${
                          router.pathname === "/confirmed-startups" ||
                          router.pathname === "/non-confirmed-startups"
                            ? "#FFFFFF"
                            : "#b5bad0"
                        }`}
                      />
                      <path
                        id="Path_37734"
                        data-name="Path 37734"
                        d="M732.255,884.827a3.3,3.3,0,0,0,.394.391c.138.118.25.116.31-.095.075-.264.172-.522.26-.783l.084,0a7.631,7.631,0,0,1,.064.809,3.932,3.932,0,0,0,.874,2.555c.442.575.868,1.164,1.263,1.771A4.961,4.961,0,0,1,736.587,892h.325a6.55,6.55,0,0,0,1.595-3.647c.029-.4.008-.794.035-1.189.052-.739.111-1.479.194-2.215a1.779,1.779,0,0,1,.224-.5l.11.026c.015.137.026.274.047.409a3.945,3.945,0,0,0,.148.789.716.716,0,0,0,.383.411c.106.03.282-.183.423-.292a.371.371,0,0,0,.073-.1,16.086,16.086,0,0,0,1.552-3.176,4.24,4.24,0,0,0-1.884-5.078c-.189-.119-.427-.322-.642-.14-.23.195-.065.464.005.705a5.625,5.625,0,0,1,.28,1.118,3.65,3.65,0,0,1-.624,2.475,1.815,1.815,0,0,1-.4.313,2.17,2.17,0,0,1-.228-.427c-.1-.369-.178-.746-.264-1.119a6.347,6.347,0,0,0-.323.873,8.213,8.213,0,0,1-.9,2.139c-.276.412-.462.414-.732.007a12.333,12.333,0,0,1-1.394-2.856,1.441,1.441,0,0,1-.065-.321c-.035-.344-.075-.371-.344-.17a.269.269,0,0,1-.446-.071,5.568,5.568,0,0,1-.332-.623c-.231-.506-.438-1.023-.681-1.523-.18-.37-.3-.4-.568-.09a10.964,10.964,0,0,0-1.08,1.457,3.7,3.7,0,0,0-.168,3.475A8.532,8.532,0,0,0,732.255,884.827Z"
                        transform="translate(-720.258 -841.44)"
                        fill={`${
                          router.pathname === "/confirmed-startups" ||
                          router.pathname === "/non-confirmed-startups"
                            ? "#FFFFFF"
                            : "#b5bad0"
                        }`}
                      />
                      <path
                        id="Path_37735"
                        data-name="Path 37735"
                        d="M693.312,851.115a14.7,14.7,0,0,1,2.474-8.212,14.025,14.025,0,0,1,1.6-1.819.769.769,0,0,1,1.336.444,37.663,37.663,0,0,0,2.5,7.467.788.788,0,0,1-.526,1.183,15.2,15.2,0,0,0-1.746.661,5.972,5.972,0,0,0-3.267,4.319c-.061.263-.108.529-.16.793a.7.7,0,0,1-.663.622.741.741,0,0,1-.8-.528A16.066,16.066,0,0,1,693.312,851.115Z"
                        transform="translate(-693.312 -815.105)"
                        fill={`${
                          router.pathname === "/confirmed-startups" ||
                          router.pathname === "/non-confirmed-startups"
                            ? "#FFFFFF"
                            : "#b5bad0"
                        }`}
                      />
                      <path
                        id="Path_37736"
                        data-name="Path 37736"
                        d="M788.084,851.526a14.092,14.092,0,0,1-.7,4.492.751.751,0,0,1-.613.567.707.707,0,0,1-.862-.612,9.577,9.577,0,0,0-.682-2.323,6.076,6.076,0,0,0-3.768-3.229c-.262-.091-.531-.166-.8-.25a.756.756,0,0,1-.489-1.085,37.854,37.854,0,0,0,2.546-7.61.772.772,0,0,1,1.267-.429,10.232,10.232,0,0,1,2.473,3.235A15.25,15.25,0,0,1,788.084,851.526Z"
                        transform="translate(-756.094 -815.127)"
                        fill={`${
                          router.pathname === "/confirmed-startups" ||
                          router.pathname === "/non-confirmed-startups"
                            ? "#FFFFFF"
                            : "#b5bad0"
                        }`}
                      />
                    </svg>
                  </div>
                  Startups
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/feeds"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/feeds">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 46.429 39.15"
                    >
                      <path
                        id="Icon_metro-feed"
                        data-name="Icon metro-feed"
                        d="M19.982,23.921a5.8,5.8,0,1,1,5.8,5.8A5.8,5.8,0,0,1,19.982,23.921ZM32.693,11.159a14.514,14.514,0,0,1,0,25.525,24.45,24.45,0,0,0,3.249-12.762A24.45,24.45,0,0,0,32.693,11.159ZM15.629,23.921a24.449,24.449,0,0,0,3.249,12.762,14.514,14.514,0,0,1,0-25.525,24.449,24.449,0,0,0-3.249,12.762Zm-8.705,0c0,7.794,2.467,14.79,6.377,19.575a23.22,23.22,0,0,1,0-39.15C9.39,9.132,6.923,16.127,6.923,23.921ZM38.27,4.347a23.22,23.22,0,0,1,0,39.149c3.91-4.785,6.377-11.781,6.377-19.575S42.181,9.132,38.27,4.347Z"
                        transform="translate(-2.571 -4.346)"
                        fill={`${
                          router.pathname === "/feeds" ? "#FFFFFF" : "#b5bad0"
                        }`}
                      />
                    </svg>
                  </div>
                  Feeds
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/jobs"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/jobs">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 31.5 31.507"
                    >
                      <path
                        id="Icon_awesome-wpforms"
                        data-name="Icon awesome-wpforms"
                        d="M31.5,5.287V30.72a3,3,0,0,1-3.038,3.037H3.037A3.013,3.013,0,0,1,0,30.712V5.287A3,3,0,0,1,3.037,2.25H28.47A3,3,0,0,1,31.5,5.287ZM28.877,30.712V5.287a.417.417,0,0,0-.408-.408h-.654L20.06,10.125,15.75,6.616l-4.3,3.509L3.691,4.873H3.037a.417.417,0,0,0-.408.408V30.713a.417.417,0,0,0,.408.408H28.47a.408.408,0,0,0,.408-.408ZM10.561,13.078v2.6H5.393v-2.6Zm0,5.231v2.623H5.393V18.309Zm.78-10.357,3.8-3.073H6.806l4.535,3.073Zm14.766,5.126v2.6H12.326v-2.6Zm0,5.231v2.623H12.326V18.309ZM20.159,7.952,24.694,4.88H16.369l3.79,3.073Zm5.948,15.6v2.623H19.118V23.555h6.989Z"
                        transform="translate(0 -2.25)"
                        fill="#b5bad0"
                      />
                    </svg>
                  </div>
                  Jobs
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/news"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/news">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 40.5 27"
                    >
                      <path
                        id="Icon_awesome-newspaper"
                        data-name="Icon awesome-newspaper"
                        d="M38.813,4.5H6.188A1.687,1.687,0,0,0,4.5,6.188V6.75H1.688A1.687,1.687,0,0,0,0,8.438V27.563A3.937,3.937,0,0,0,3.938,31.5H37.125A3.375,3.375,0,0,0,40.5,28.125V6.188A1.687,1.687,0,0,0,38.813,4.5ZM3.938,28.125a.563.563,0,0,1-.562-.562V10.125H4.5V27.563A.563.563,0,0,1,3.938,28.125ZM20.531,27H9.844A.844.844,0,0,1,9,26.156v-.562a.844.844,0,0,1,.844-.844H20.531a.844.844,0,0,1,.844.844v.563A.844.844,0,0,1,20.531,27Zm14.625,0H24.469a.844.844,0,0,1-.844-.844v-.562a.844.844,0,0,1,.844-.844H35.156a.844.844,0,0,1,.844.844v.563A.844.844,0,0,1,35.156,27ZM20.531,20.25H9.844A.844.844,0,0,1,9,19.406v-.562A.844.844,0,0,1,9.844,18H20.531a.844.844,0,0,1,.844.844v.563A.844.844,0,0,1,20.531,20.25Zm14.625,0H24.469a.844.844,0,0,1-.844-.844v-.562A.844.844,0,0,1,24.469,18H35.156a.844.844,0,0,1,.844.844v.563A.844.844,0,0,1,35.156,20.25Zm0-6.75H9.844A.844.844,0,0,1,9,12.656V9.844A.844.844,0,0,1,9.844,9H35.156A.844.844,0,0,1,36,9.844v2.813A.844.844,0,0,1,35.156,13.5Z"
                        transform="translate(0 -4.5)"
                        fill="#b5bad0"
                      />
                    </svg>
                  </div>
                  News
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/blog"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/blog">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 36 31.5"
                    >
                      <path
                        id="Icon_awesome-table"
                        data-name="Icon awesome-table"
                        d="M32.625,2.25H3.375A3.375,3.375,0,0,0,0,5.625v24.75A3.375,3.375,0,0,0,3.375,33.75h29.25A3.375,3.375,0,0,0,36,30.375V5.625A3.375,3.375,0,0,0,32.625,2.25Zm-16.875,27H4.5V22.5H15.75Zm0-11.25H4.5V11.25H15.75ZM31.5,29.25H20.25V22.5H31.5ZM31.5,18H20.25V11.25H31.5Z"
                        transform="translate(0 -2.25)"
                        fill={`${
                          router.pathname === "/blog" ? "#FFFFFF" : "#b5bad0"
                        }`}
                      />
                    </svg>
                  </div>
                  Blog
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/mail-center"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/mail-center">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      id="Icon_ionic-ios-mail"
                      data-name="Icon ionic-ios-mail"
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 39.091 27.063"
                    >
                      <path
                        id="Path_37746"
                        data-name="Path 37746"
                        d="M42.147,10.372l-10.111,10.3a.182.182,0,0,0,0,.263l7.076,7.536a1.219,1.219,0,0,1,0,1.729,1.225,1.225,0,0,1-1.729,0l-7.048-7.508a.193.193,0,0,0-.273,0l-1.72,1.748a7.566,7.566,0,0,1-5.394,2.274,7.718,7.718,0,0,1-5.507-2.34l-1.654-1.682a.193.193,0,0,0-.273,0L8.468,30.2a1.225,1.225,0,0,1-1.729,0,1.219,1.219,0,0,1,0-1.729l7.076-7.536a.2.2,0,0,0,0-.263L3.694,10.372a.185.185,0,0,0-.319.132V31.111a3.016,3.016,0,0,0,3.007,3.007H39.459a3.016,3.016,0,0,0,3.007-3.007V10.5A.188.188,0,0,0,42.147,10.372Z"
                        transform="translate(-3.375 -7.054)"
                        fill={`${
                          router.pathname === "/mail-center"
                            ? "#FFFFFF"
                            : "#b5bad0"
                        }`}
                      />
                      <path
                        id="Path_37747"
                        data-name="Path 37747"
                        d="M22.634,25.081a5.109,5.109,0,0,0,3.674-1.541L41.052,8.533a2.953,2.953,0,0,0-1.861-.658H6.086a2.934,2.934,0,0,0-1.861.658L18.97,23.54A5.109,5.109,0,0,0,22.634,25.081Z"
                        transform="translate(-3.089 -7.875)"
                        fill={`${
                          router.pathname === "/mail-center"
                            ? "#FFFFFF"
                            : "#b5bad0"
                        }`}
                      />
                    </svg>
                  </div>
                  Mail Center
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/sessions"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/sessions">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 31.5 31.507"
                    >
                      <path
                        id="Icon_awesome-wpforms"
                        data-name="Icon awesome-wpforms"
                        d="M31.5,5.287V30.72a3,3,0,0,1-3.038,3.037H3.037A3.013,3.013,0,0,1,0,30.712V5.287A3,3,0,0,1,3.037,2.25H28.47A3,3,0,0,1,31.5,5.287ZM28.877,30.712V5.287a.417.417,0,0,0-.408-.408h-.654L20.06,10.125,15.75,6.616l-4.3,3.509L3.691,4.873H3.037a.417.417,0,0,0-.408.408V30.713a.417.417,0,0,0,.408.408H28.47a.408.408,0,0,0,.408-.408ZM10.561,13.078v2.6H5.393v-2.6Zm0,5.231v2.623H5.393V18.309Zm.78-10.357,3.8-3.073H6.806l4.535,3.073Zm14.766,5.126v2.6H12.326v-2.6Zm0,5.231v2.623H12.326V18.309ZM20.159,7.952,24.694,4.88H16.369l3.79,3.073Zm5.948,15.6v2.623H19.118V23.555h6.989Z"
                        transform="translate(0 -2.25)"
                        fill="#b5bad0"
                      />
                    </svg>
                  </div>
                  Sessions
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg ${
                router.pathname === "/analytics"
                  ? "bg-green-600 text-white shadow-lg"
                  : null
              }`}
            >
              <Link href="/analytics">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.95"
                      height="22.95"
                      viewBox="0 0 29.25 29.25"
                    >
                      <path
                        id="Icon_ionic-ios-analytics"
                        data-name="Icon ionic-ios-analytics"
                        d="M18,3.375A14.625,14.625,0,1,0,32.625,18,14.623,14.623,0,0,0,18,3.375ZM5.344,18a12.658,12.658,0,0,1,25.305-.534,4.281,4.281,0,0,0-3.08-1.687c-1.934,0-2.932,1.807-3.586,3-.1.176-.19.345-.274.492A3.48,3.48,0,0,1,20.721,21.3a2.846,2.846,0,0,1-2.264-2.025c-.647-1.554-2.046-3.22-3.72-3.459a3.348,3.348,0,0,0-3.143,1.5c-.225.281-.485.661-.78,1.1-.731,1.09-1.842,2.728-2.679,2.869a3.213,3.213,0,0,1-2.559-.865A12.61,12.61,0,0,1,5.344,18Z"
                        transform="translate(-3.375 -3.375)"
                        fill="#b5bad0"
                      />
                    </svg>
                  </div>
                  Analytics
                </a>
              </Link>
            </li>
            <li
              className={`rounded-md py-2 icon hover:bg-green-600 hover:text-white hover:shadow-lg`}
            >
              <Link href="/api/logout">
                <a>
                  <div className="w-8 mx-auto mt-2 mb-1">
                    <svg
                      id="Group_35946"
                      data-name="Group 35946"
                      xmlns="http://www.w3.org/2000/svg"
                      width="32.029"
                      height="22.715"
                      viewBox="0 0 41.029 43.715"
                    >
                      <path
                        id="Path_37737"
                        data-name="Path 37737"
                        d="M955.4,815.967h-5.19c-.021-.03-.042-.047-.042-.065a16.251,16.251,0,0,1,.635-6.118,10.435,10.435,0,0,1,9.732-6.779q7.747-.009,15.5,0a10.394,10.394,0,0,1,10.418,10.378q.027,11.483,0,22.966a10.412,10.412,0,0,1-9.169,10.292,10.867,10.867,0,0,1-1.3.07q-7.682.008-15.365,0a10.428,10.428,0,0,1-10.459-10.447q0-1.337,0-2.675v-.366H955.4c0,.139,0,.266,0,.392.009,1.022-.035,2.048.037,3.065a5.1,5.1,0,0,0,5.075,4.737q7.779.023,15.56,0a5.148,5.148,0,0,0,5.134-5.233q.008-8.824,0-17.649,0-2.48,0-4.959a5.161,5.161,0,0,0-5.277-5.288q-7.633-.006-15.267,0a5.158,5.158,0,0,0-5.263,5.269C955.4,814.352,955.4,815.144,955.4,815.967Z"
                        transform="translate(-945.432 -803)"
                        fill="#b5bad0"
                      />
                      <path
                        id="Path_37738"
                        data-name="Path 37738"
                        d="M976.456,824.685,962.83,835.18v-6.5h-17.4v-8.394h17.385V814.39Z"
                        transform="translate(-945.432 -803)"
                        fill="#b5bad0"
                      />
                    </svg>
                  </div>
                  Logout
                </a>
              </Link>
            </li>
          </ul>
        </div>
        <div className="w-full">
          <div className="ml-40 mr-8 mt-4">{children}</div>
        </div>
      </div>
    </div>
  );
}

export default Layout;

const AuthLayout = ({ children }) => {
  return (
    <>
      <div className="flex justify-start items-center">
        <img src="/logo.svg" width="50" height="50" className="ml-5 absolute" />
        <div className="bg-white flex justify-start items-center w-full p-5 ml-14 shadow-sm">
          <img
            src="/img/burger.png"
            width="19"
            height="10"
            className="mr-5 ml-8"
          />
          <p className="font-bold text-2xl text-slate-500 poppines-bold">
            Admin Dashboard
          </p>
        </div>
      </div>

      <div className="max-w-4xl	mt-16 mx-auto">
        <div className="flex w-full justify-center">
          <div className="bg-green-900 w-3/5 py-24 hidden xl:block">
            <img
              src="/img/logo-auth.png"
              height="300"
              width="200"
              className="mx-auto"
            />
            <img
              src="/img/Bantaba.png"
              width="250"
              height="50"
              className="mx-auto mt-10"
            />
          </div>
          <div className="form bg-white w-4/5 md:w-2/5 xl:w-2/5 ml-3 flex flex-col justify-center p-4">{children}</div>
        </div>
      </div>
    </>
  );
};

export default AuthLayout;

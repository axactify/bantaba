import React from "react";
import Layout from "../components/Layout";
import withSession from "../lib/session";

const Black = () => {
  return (
    <>
      <Layout user={user}>
        <p>Blank</p>
      </Layout>
    </>
  );
};

export const getServerSideProps = withSession(async (ctx) => {
  if (ctx.req.session.get("user") === undefined) {
    ctx.res.setHeader("location", "/login");
    ctx.res.statusCode = 302;
    ctx.res.end();
    return { props: {} };
  }

  const user = ctx.req.session.get("user");

  return {
    props: {
      user,
    },
  };
});

export default Black;

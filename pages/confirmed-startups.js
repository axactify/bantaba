import React, { useEffect, useState } from "react";
import Layout from "../components/Layout";
import Link from "next/link";
import { Calendar } from "react-multi-date-picker";
import Modal from "react-modal";
import ViewStartup from "../components/ViewStartup";
import { useDispatch, useSelector } from "react-redux";
import { add, open } from "../features/modal/modalSlice";
import ConfirmDeletionStartup from "../components/ConfirmDeletionStartup";
import RejectStartup from "../components/RejectStartup";
import withSession from "../lib/session";
import {
  confirmStartup,
  fetchLastMonthRecords,
  fetchSingleRecord,
  fetchStartupDateRange,
  fetchStartups,
  removeError,
  resetFilter,
} from "../features/startups/startupSlice";
import Image from "next/image";
import ConfirmStartupLoadMoreBtn from "../components/ConfirmStartupLoadMoreBtn";
import Moment from "react-moment";
import { getName as getCountryName } from "country-list";
import { getLanguageName, standardDateFormat } from "../utils/helper";

const ConfirmedStartups = ({ user }) => {
  const dispatch = useDispatch();
  const [openFilter, setOpenFilter] = useState(false);
  const [openCalandar, setOpenCalender] = useState(false);
  const [confirmed, setConfirmed] = useState(1);
  const modalIsOpen = useSelector((state) => state.modal.open);
  const modalName = useSelector((state) => state.modal.name);
  const isLoading = useSelector((state) => state.startup.loading);
  const startups = useSelector((state) => state.startup.startups);
  const isError = useSelector((state) => state.startup.isError);
  const message = useSelector((state) => state.startup.message);
  const filterCurrentPage = useSelector(
    (state) => state.startup.filterCurrentPage
  );
  const [range, setRange] = useState("");

  useEffect(() => {
    dispatch(fetchStartups(confirmed));
  }, [confirmed]);

  useEffect(() => {
    if (range.length > 1) {
      const x = new Date(range[0]);
      const y = new Date(range[1]);
      const startDate = standardDateFormat(x);
      const endDate = standardDateFormat(y);
      const payload = {
        startDate,
        endDate,
        page: 0,
      };
      dispatch(fetchStartupDateRange(payload));
      setOpenCalender(false);
      setOpenFilter(false);
      setRange("");
    }
  }, [range]);

  const toggleDropdown = () => {
    setOpenFilter(!openFilter);
    setOpenCalender(false);
  };

  return (
    <>
      <Layout user={user}>
        {isError && (
          <div className="p-4 bg-red-600 rounded-md text-stone-100 flex justify-between items-center">
            <p> Error: {message} </p>
            <button
              className="bg-red-800 px-4 py-1 rounded-md hover:bg-red-900"
              onClick={() => dispatch(removeError())}
            >
              Close
            </button>
          </div>
        )}

        <div className="xl:flex justify-between">
          <p className="text-2xl xl:text-3xl font-bold font-lato mb-10 mt-8">
            {confirmed ? "Confirmed" : "Non Confirmed"} Startups
          </p>

          <div className="flex items-center cursor-pointer mb-10 xl:mb-0">
            <div className="flex flex-col items-center cursor-pointer select-none">
              <div className="flex items-center">
                <span>
                  <div
                    className="flex items-center justify-between mb-2 mr-8"
                    onClick={() => toggleDropdown()}
                  >
                    <span className="text-lg font-semibold">Filter</span>
                    <img
                      src="/img/Icon feather-chevron-down.png"
                      className="ml-3 mt-1"
                      alt="Arrow Down"
                      width="15"
                      height="15"
                    />
                  </div>

                  <div
                    className={`absolute xl:right-10 mt-1 z-50 ${
                      openCalandar ? "block" : "hidden"
                    }`}
                  >
                    <Calendar
                      range
                      numberOfMonths={2}
                      plugins={[]}
                      value={range}
                      onChange={setRange}
                    />
                  </div>

                  <div
                    className={`dropdown bg-white shadow-md p-4 absolute z-30 ${
                      openCalandar === false && openFilter ? "block" : "hidden"
                    }`}
                  >
                    <ul className="space-y-3">
                      <li className="text-slate-400 hover:text-slate-600">
                        <button
                          onClick={() => {
                            setOpenFilter(false);
                            dispatch(fetchLastMonthRecords(filterCurrentPage));
                          }}
                        >
                          Last Month
                        </button>
                      </li>
                      <li
                        onClick={() => {
                          setOpenFilter(true);
                          setOpenCalender(true);
                        }}
                        className="text-slate-400  hover:text-slate-600"
                      >
                        <span>Date Range</span>
                      </li>
                    </ul>
                  </div>
                </span>

                <button
                  onClick={() => {
                    dispatch(resetFilter());
                    dispatch(fetchStartups(confirmed));
                  }}
                  className="ml-5 bg-slate-200 hover:bg-slate-300 rounded-md px-3 py-1 text-zinc-700"
                >
                  Reset Filter
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="p-8 bg-white shadow-xl mb-5">
          <div className="flex flex-col md:flex-row space-y-4 md:space-y-0">
            <button
              onClick={() => setConfirmed(1)}
              className={`py-2 px-4 text-sm ${
                confirmed === 1
                  ? "bg-green-600 text-white"
                  : "bg-slate-300 hover:bg-slate-200"
              }  rounded-full md:mr-4`}
            >
              Confirmed Startups
            </button>
            <button
              onClick={() => setConfirmed(0)}
              className={`py-2 px-4 text-sm ${
                confirmed === 0
                  ? "bg-green-600 text-white"
                  : "bg-slate-300 hover:bg-slate-200"
              }  rounded-full md:mr-4`}
            >
              Non Confirmed Startups
            </button>
          </div>
        </div>

        {isLoading && (
          <div className="flex justify-center items-center mt-48">
            <div className="fixed left-2/4">
              <Image src="/img/loading.gif" width="50" height="50" />
            </div>
          </div>
        )}

        {isLoading === false && startups && startups.length < 1 && (
          <div className="flex justify-center items-center mt-48">
            <div className="fixed left-2/4">
              <p>There is no startup</p>
            </div>
          </div>
        )}

        {isLoading === false &&
          startups &&
          startups.map((startup) => (
            <div
              key={startup?.startups_id}
              className="grid grid-cols-6 p-4 bg-white shadow-md justify-between items-center rounded-md mb-5 hover:bg-slate-200 hover:cursor-pointer"
            >
              <div className="flex justify-start col-span-full  xl:col-span-1">
                <div>
                  <Image
                    src={startup?.user_avatar || "/img/avatar_default.jpg"}
                    width="40"
                    height="40"
                    className="rounded-full border-2 border-slate-500"
                  />
                </div>
                <div className="ml-6">
                  <p className="text-lg">{startup?.startups_startupname}</p>

                  {startup?.startups_confirmed === 1 && (
                    <span className="text-[10px] border-2 rounded-md bg-emerald-100  border-blue-600 text-blue-600 hover:bg-blue-100 px-2 font-semibold col-span-full">
                      Score: {startup?.startups_score}
                    </span>
                  )}

                  {startup?.startups_confirmed === 0 && (
                    <span className="text-[10px] border-2 rounded-md bg-red-200 border-red-600 text-red-600 px-2 font-semibold col-span-full">
                      Not Confirmed
                    </span>
                  )}
                </div>
              </div>

              <div className="col-span-full xl:col-span-3">
                <div className="grid grid-cols-3">
                  <div className="xl:ml-5">
                    <p className="text-lg">Lives In</p>
                    <p className="uppercase text-sm text-slate-500">
                      {getCountryName(startup?.startups_location)}
                    </p>
                  </div>

                  <div className="xl:ml-5">
                    <p className="text-lg">Sector</p>
                    <p className="text-sm text-slate-500 break-words capitalize">
                      {startup?.startups_sector}
                    </p>
                  </div>

                  <div className="xl:ml-5 mb-4 xl:mb-0">
                    <p className="text-lg">Join date</p>
                    <p className="uppercase text-sm text-slate-500">
                      <Moment format="DD/MM/YYYY">
                        {startup?.startups_created_at}
                      </Moment>
                    </p>
                  </div>
                </div>
              </div>

              <div className="mb-4 xl:mb-0 flex items-center justify-between col-span-full xl:col-span-1">
                {startup?.startupProfile_preferedLanguage === "" ? (
                  <div>
                    <p className="text-lg">Language</p>
                    <span>-</span>
                  </div>
                ) : (
                  <div className="flex flex-col justify-start">
                    <p className="text-lg mt-3">Language</p>
                    <span className="px-4 py-1 mt-2 text-sm rounded-md bg-[#D1ECF1] text-[#125E7F]">
                      {getLanguageName(startup.startupProfile_preferedLanguage)}
                    </span>
                  </div>
                )}

                <div>
                  <Image
                    src="/img/Icon awesome-eye.png"
                    width="30"
                    height="20"
                    className="cursor-pointer"
                    onClick={() => {
                      const payload = {
                        id: startup?.startups_id,
                        startup_name: startup?.startups_startupname,
                      };
                      dispatch(fetchSingleRecord(payload));
                      dispatch(open("ViewStartup"));
                    }}
                  />
                </div>
              </div>

              <div className="flex items-center justify-between xl:justify-end col-span-full xl:col-span-1">
                <div className="flex flex-col">
                  {confirmed === 0 && (
                    <span
                      onClick={() =>
                        dispatch(confirmStartup(startup?.startups_id))
                      }
                      className="px-5 py-2 text-sm rounded-md mb-4 bg-green-200 text-green-500 font-semibold hover:bg-green-300 cursor-pointer"
                    >
                      Confirm
                    </span>
                  )}

                  <span
                    onClick={() => {
                      const payload = {
                        id: startup?.startups_id,
                        email: startup?.user_email,
                        startup_name: startup?.startups_startupname,
                        lang: startup?.startupProfile_preferedLanguage || "en",
                        email: startup?.user_email,
                      };
                      dispatch(add(payload));
                      dispatch(fetchSingleRecord(payload));
                      dispatch(open("RejectStartup"));
                    }}
                    className="px-4 py-2 text-sm rounded-md bg-red-200 text-red-500 font-semibold hover:bg-red-300 cursor-pointer"
                  >
                    Reject
                  </span>
                </div>
                <div className="ml-5">
                  <span
                    onClick={() => {
                      const payload = {
                        id: startup?.startups_id,
                      };
                      dispatch(add(payload));
                      dispatch(open("ConfirmDeletionStartup"));
                    }}
                    className="px-4 py-2 text-sm rounded-md bg-red-500 text-white hover:bg-red-600 cursor-pointer"
                  >
                    Delete
                  </span>
                </div>
              </div>
            </div>
          ))}

        {isLoading === false && startups && startups.length >= 20 && (
          <ConfirmStartupLoadMoreBtn type={confirmed} />
        )}

        {/* Modals */}
        <Modal
          isOpen={modalIsOpen && modalName === "ViewStartup"}
          onAfterOpen={() => {
            document.body.style.overflow = "hidden";
          }}
          style={{
            content: {
              height: "auto",
              border: "none",
              background: "transparent !important",
              inset: "0px",
            },
          }}
          contentLabel="ViewStartup"
        >
          <ViewStartup />
        </Modal>
        <Modal
          isOpen={modalIsOpen && modalName === "RejectStartup"}
          onAfterOpen={() => {
            document.body.style.overflow = "hidden";
          }}
          style={{
            content: {
              height: "auto",
              maxWidth: "800px",
              margin: "0 auto",
              border: "none",
              background: "transparent !important",
              inset: "0px",
            },
          }}
          contentLabel="Reject Startup"
        >
          <RejectStartup />
        </Modal>
        <Modal
          isOpen={modalIsOpen && modalName === "ConfirmDeletionStartup"}
          onAfterOpen={() => {
            document.body.style.overflow = "hidden";
          }}
          style={{
            content: {
              height: "auto",
              maxWidth: "800px",
              margin: "0 auto",
              border: "none",
              background: "transparent !important",
              inset: "0px",
            },
          }}
          contentLabel="ConfirmDeletionStartup"
        >
          <ConfirmDeletionStartup />
        </Modal>
      </Layout>
    </>
  );
};

export const getServerSideProps = withSession(async (ctx) => {
  if (ctx.req.session.get("user") === undefined) {
    ctx.res.setHeader("location", "/login");
    ctx.res.statusCode = 302;
    ctx.res.end();
    return { props: {} };
  }

  const user = ctx.req.session.get("user");

  return {
    props: {
      user,
    },
  };
});

export default ConfirmedStartups;

import "../styles/globals.css";
import Modal from "react-modal";
import { store } from "../store";
import { Provider } from "react-redux";

Modal.setAppElement("#__next");

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;

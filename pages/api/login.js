import axios from "axios";
import withSession from "../../lib/session";

export default withSession(async (req, res) => {
  const { email, password } = await req.body;
  const payload = {
    email,
    password,
  };
  const url = "https://dashapi-dev.ourbantaba.com/users/login";
  try {
    const response = await axios.post(url, payload);
    req.session.set("user", response.data);
    await req.session.save();

    res.json({
      success: true,
      message: "Login Successfull",
    });
  } catch (error) {
    const { response: fetchResponse } = error;
    const message =
      (error.response && error.response.data && error.response.data.message) ||
      error.message ||
      error.toString();

    res.status(fetchResponse?.status || 500).json({ message });
  }
});

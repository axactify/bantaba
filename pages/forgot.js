import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ResetPassword from "../components/ResetPassword";
import { removeError, forgotPassword } from "../features/auth/authSlice";
import withSession from "../lib/session";
import Swal from "sweetalert2";
import { BiLoaderAlt } from "react-icons/bi";
import AuthLayout from "../components/AuthLayout";

export default function Forgot() {
  const dispatch = useDispatch();
  const isError = useSelector((state) => state.auth.isError);
  const message = useSelector((state) => state.auth.message);
  const isFound = useSelector((state) => state.auth.isFound);
  const isProcessing = useSelector((state) => state.auth.isProcessing);
  const [email, setEmail] = useState("");

  const submitForm = (e) => {
    e.preventDefault();
    const payload = {
      email,
    };
    setEmail("");
    dispatch(forgotPassword(payload));
    Swal.fire({
      icon: "info",
      text: "You will receive email, if your email is registered with our website.",
      timer: 1500,
    });
  };

  return (
    <>
      <AuthLayout>
        <div className="flex flex-row justify-center items-center">
          {isFound ? (
            <ResetPassword />
          ) : (
            <form onSubmit={submitForm} className="w-64 mx-auto">
              <p className="text-center mb-16 text-2xl">Reset Password</p>
              <p className="text-sm">Email</p>
              <input
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="border-b-2 border-slate-500 outline-0 w-full h-7"
                required
              />
              <div className="text-center">
                <button
                  type="submit"
                  className="uppercase px-12 py-2 font-semibold bg-green-600 hover:bg-green-700 text-white mt-8 rounded-md"
                  disabled={isProcessing}
                >
                  <span className="flex items-center justify-between">
                    {isProcessing && (
                      <i className="icon-spin mr-2">
                        <BiLoaderAlt />
                      </i>
                    )}
                    Submit
                  </span>
                </button>
              </div>
            </form>
          )}
        </div>
      </AuthLayout>
    </>
  );
}

export const getServerSideProps = withSession(async (ctx) => {
  if (ctx.req.session.get("user")) {
    ctx.res.setHeader("location", "/dashboard");
    ctx.res.statusCode = 302;
    ctx.res.end();
    return { props: {} };
  }

  return {
    props: {},
  };
});

import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginUser, removeError } from "../features/auth/authSlice";
import withSession from "../lib/session";
import { BiLoaderAlt } from "react-icons/bi";
import Swal from "sweetalert2";
import AuthLayout from "../components/AuthLayout";

export default function Login() {
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const isError = useSelector((state) => state.auth.isError);
  const message = useSelector((state) => state.auth.message);
  const isLogin = useSelector((state) => state.auth.isLogin);
  const isProcessing = useSelector((state) => state.auth.isProcessing);
  const router = useRouter();

  useEffect(() => {
    if (isLogin) {
      router.push("/dashboard");
    }
  }, [isLogin]);

  const submitForm = (e) => {
    e.preventDefault();

    const payload = {
      email,
      password,
    };
    dispatch(loginUser(payload));
  };

  return (
    <>
      <AuthLayout>
        <form onSubmit={submitForm} className="mx-auto">
          <p className="text-center my-16 text-2xl">Login your account</p>
          <p className="text-sm">Email</p>
          <input
            type="email"
            onChange={(e) => setEmail(e.target.value)}
            className="border-b-2 border-slate-500 outline-0 w-full h-7"
            required
          />
          <p className="mt-8 text-sm">Password</p>
          <input
            type="password"
            onChange={(e) => setPassword(e.target.value)}
            className="border-b-2 border-slate-500 outline-0 w-full h-7"
            minLength={3}
            required
          />
          <div className="text-center">
            <button
              type="submit"
              className="uppercase px-12 py-2 font-semibold bg-green-600 hover:bg-green-700 text-white mt-8 rounded-md"
              disabled={isProcessing}
            >
              <span className="flex items-center justify-between">
                {isProcessing && (
                  <i className="icon-spin mr-2">
                    <BiLoaderAlt />
                  </i>
                )}
                Login
              </span>
            </button>
          </div>
          <p className="text-center text-red-500 mt-12 text-sm">
            <Link href="/forgot">
              <a>Forgot Password?</a>
            </Link>
            <hr className="border-red-500 w-28 mx-auto" />
          </p>
        </form>
      </AuthLayout>
    </>
  );
}

export const getServerSideProps = withSession(async (ctx) => {
  if (ctx.req.session.get("user")) {
    ctx.res.setHeader("location", "/dashboard");
    ctx.res.statusCode = 302;
    ctx.res.end();
    return { props: {} };
  }

  return {
    props: {},
  };
});

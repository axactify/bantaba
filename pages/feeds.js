import React, { useEffect, useState } from "react";
import Layout from "../components/Layout";
import ReactFlagsSelect from "react-flags-select";
import { useDispatch, useSelector } from "react-redux";
import {
  approveFeed,
  deleteFeed,
  fetchFeeds,
  rejectFeed,
  removeError,
} from "../features/feeds/feedSlice";
import Moment from "react-moment";
import FeedLoadMoreBtn from "../components/FeedLoadMoreBtn";
import withSession from "../lib/session";
import Image from "next/image";

const Feeds = ({ user }) => {
  const [country, setCountry] = useState("US");
  const dispatch = useDispatch();
  const feeds = useSelector((state) => state.feed.feeds);
  const isLoading = useSelector((state) => state.feed.loading);
  const language = country === "US" ? "EN" : "FR";
  const isError = useSelector((state) => state.feed.isError);
  const message = useSelector((state) => state.feed.message);

  useEffect(() => {
    dispatch(fetchFeeds(language));
  }, [language]);

  return (
    <>
      <Layout user={user}>
        {isError && (
          <div className="p-4 bg-red-600 rounded-md text-stone-100 flex justify-between items-center">
            <p> Error: {message} </p>
            <button
              className="bg-red-800 px-4 py-1 rounded-md hover:bg-red-900"
              onClick={() => dispatch(removeError())}
            >
              Close
            </button>
          </div>
        )}
        <div className="flex justify-between items-center mb-4">
          <p className="text-3xl font-bold font-lato mb-10 mt-8">
            Approval Feeds
          </p>
          <ReactFlagsSelect
            selected={country}
            onSelect={(code) => setCountry(code)}
            countries={["US", "FR"]}
            customLabels={{
              US: { primary: "EN" },
              FR: { primary: "FR" },
            }}
            placeholder="Select a Language"
            className="w-28 bg-white h-11"
          />
        </div>

        {isLoading ? (
          <div className="flex justify-center items-center mt-48">
            <div className="fixed left-2/4">
              <Image src="/img/loading.gif" width="50" height="50" />
            </div>
          </div>
        ) : (
          <React.Fragment>
            <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-2 gap-10">
              {feeds &&
                feeds.map((feed) => (
                  <div
                    key={feed.id}
                    className="shadow-md border-2 hover:border-green-500 cursor-pointer  bg-white flex-col justify-between items-center"
                  >
                    <div className="border-l-4 border-green-600 h-7 absolute mt-16"></div>
                    <div className="px-7">
                      <div className="flex justify-between mb-3">
                        {feed.hide ? (
                          <button className="my-3 px-4 py-1 bg-red-200 text-sm text-orange-700 rounded-md flex items-center">
                            <img
                              src="/img/Icon awesome-lock.png"
                              width="15"
                              height="15"
                            />

                            <span className="ml-2">Hidden</span>
                          </button>
                        ) : (
                          <button className="my-3 px-4 py-1 bg-purple-300 text-sm text-purple-700 rounded-md flex items-center">
                            <img
                              src="/img/Icon awesome-globe-africa.png"
                              width="18"
                              height="18"
                            />
                            <span className="ml-2">Public</span>
                          </button>
                        )}
                      </div>

                      <div className="flex justify-between items-center mb-4">
                        <p className="text-xl font-semibold text-green-600">
                          {feed.user?.name}
                        </p>

                        <div className="flex items-center">
                          <img
                            src="/img/Group 9721.png"
                            width="15"
                            height="15"
                          />
                          <p className="text-sm ml-1">
                            <Moment format="YYYY/MM/DD">
                              {feed.updated_at}
                            </Moment>
                          </p>
                        </div>
                      </div>

                      <div className="grid grid-col-1 xl:grid-cols-4 gap-10">
                        {feed.images.length > 0 && (
                          <div className="col-span-2">
                            <Image
                              src={feed.images[0].image}
                              alt="feed cover"
                              width="500"
                              height="300"
                              className="rounded-lg"
                            />
                          </div>
                        )}

                        <div
                          className={`mb-5 ${
                            feed.images.length > 0 ? "col-span-2" : "col-span-3"
                          }`}
                        >
                          <p className="my-3 break-words">{feed.post}</p>
                        </div>
                      </div>

                      <div className="flex flex-col xl:flex-row justify-between items-center my-4">
                        <button
                          onClick={() => dispatch(approveFeed(feed.id))}
                          className="mb-2 px-12 py-4 border-2 bg-slate-100 border-green-300 text-sm text-green-500 rounded-md hover:bg-[#349E4D] hover:text-white"
                        >
                          Approve
                        </button>
                        <button
                          onClick={() => dispatch(rejectFeed(feed.id))}
                          className="mb-2 px-12 py-4 border-2 border-red-500 text-red-500 rounded-md hover:bg-[#B52F2F] hover:text-white"
                        >
                          Reject
                        </button>
                        <button
                          onClick={() => dispatch(deleteFeed(feed.id))}
                          className="mb-2 px-12 py-4 border-2 border-red-400 bg-red-200  text-red-700 rounded-md hover:bg-[#F72C2C] hover:text-white"
                        >
                          Delete
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
            <FeedLoadMoreBtn language={language} />
          </React.Fragment>
        )}
      </Layout>
    </>
  );
};

export const getServerSideProps = withSession(async (ctx) => {
  if (ctx.req.session.get("user") === undefined) {
    ctx.res.setHeader("location", "/login");
    ctx.res.statusCode = 302;
    ctx.res.end();
    return { props: {} };
  }

  const user = ctx.req.session.get("user");

  return {
    props: {
      user,
    },
  };
});

export default Feeds;

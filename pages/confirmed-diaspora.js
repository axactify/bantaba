import React, { useEffect, useState } from "react";
import Layout from "../components/Layout";
import Link from "next/link";
import Modal from "react-modal";
import { useDispatch, useSelector } from "react-redux";
import ViewDiasPora from "../components/ViewDiaspora";
import { add, open } from "../features/modal/modalSlice";
import { Calendar } from "react-multi-date-picker";
import withSession from "../lib/session";
import {
  fetchLastMonthRecords,
  fetchRecords,
  fetchSingleRecord,
  fetchDiasporaDateRange,
  removeError,
  confirmDiaspora,
} from "../features/diaspora/diasporaSlice";
import Image from "next/image";
import ConfirmDiasporaLoadMoreBtn from "../components/ConfirmDiasporaLoadMoreBtn";
import ConfirmDeletionDaispora from "../components/ConfirmDeletionDaispora";
import RejectDiaspora from "../components/RejectDiaspora";
import { getName as getCountryName } from "country-list";
import Moment from "react-moment";
import { getLanguageName, standardDateFormat } from "../utils/helper";

const ConfirmedDiaspora = ({ user }) => {
  const dispatch = useDispatch();
  const modalIsOpen = useSelector((state) => state.modal.open);
  const modalName = useSelector((state) => state.modal.name);
  const isLoading = useSelector((state) => state.diaspora.loading);
  const diaspora = useSelector((state) => state.diaspora.diaspora);
  const isError = useSelector((state) => state.diaspora.isError);
  const message = useSelector((state) => state.diaspora.message);
  const [openFilter, setOpenFilter] = useState(false);
  const [openCalander, setOpenCalender] = useState(false);
  const [confirmed, setConfirmed] = useState(1);
  const [range, setRange] = useState("");

  const toggleDropdown = () => {
    setOpenFilter(!openFilter);
    setOpenCalender(false);
  };

  useEffect(() => {
    dispatch(fetchRecords(confirmed));
  }, [confirmed]);

  useEffect(() => {
    if (range.length > 1) {
      const x = new Date(range[0]);
      const y = new Date(range[1]);
      const startDate = standardDateFormat(x);
      const endDate = standardDateFormat(y);
      const payload = {
        startDate,
        endDate,
        page: 0,
      };
      dispatch(fetchDiasporaDateRange(payload));
      setOpenCalender(false);
      setOpenFilter(false);
      setRange("");
    }
  }, [range]);

  return (
    <>
      <Layout user={user}>
        {isError && (
          <div className="p-4 bg-red-600 rounded-md text-stone-100 flex justify-between items-center">
            <p> Error: {message} </p>
            <button
              className="bg-red-800 px-4 py-1 rounded-md hover:bg-red-900"
              onClick={() => dispatch(removeError())}
            >
              Close
            </button>
          </div>
        )}

        <div className="xl:flex justify-between">
          <p className="text-2xl xl:text-3xl font-bold font-lato mb-10 mt-8">
            {confirmed ? "Confirmed" : "Non Confirmed"} Diaspora
          </p>

          <div className="flex items-center cursor-pointer">
            <div className="flex flex-col items-center cursor-pointer select-none">
              <div className="flex items-center">
                <span>
                  <div
                    className="flex items-center justify-between mb-2 mr-8"
                    onClick={() => toggleDropdown()}
                  >
                    <span className="text-lg font-semibold">Filter</span>
                    <img
                      src="/img/Icon feather-chevron-down.png"
                      className="ml-3 mt-1"
                      alt="Arrow Down"
                      width="15"
                      height="15"
                    />
                  </div>

                  <div
                    className={`absolute right-10 mt-1 z-50 ${
                      openCalander ? "block" : "hidden"
                    }`}
                  >
                    <Calendar
                      range
                      numberOfMonths={2}
                      plugins={[]}
                      value={range}
                      onChange={setRange}
                    />
                  </div>

                  <div
                    className={`dropdown bg-white shadow-md p-4 absolute  ${
                      openFilter ? "block" : "hidden"
                    }`}
                  >
                    <ul className="space-y-3">
                      <li className="text-slate-400 hover:text-slate-600">
                        <button
                          onClick={() => {
                            setOpenFilter(false);
                            dispatch(fetchLastMonthRecords(confirmed));
                          }}
                        >
                          Last Month
                        </button>
                      </li>
                      <li
                        onClick={() => {
                          setOpenFilter(true);
                          setOpenCalender(true);
                        }}
                        className="text-slate-400  hover:text-slate-600"
                      >
                        <span>Date Range</span>
                      </li>
                    </ul>
                  </div>
                </span>

                <button
                  onClick={() => dispatch(fetchRecords(confirmed))}
                  className="ml-5 bg-slate-200 hover:bg-slate-300 rounded-md px-3 py-1 text-zinc-700"
                >
                  Reset Filter
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="p-8 bg-white shadow-xl mb-5">
          <div className="flex flex-col md:flex-row space-y-4 md:space-y-0">
            <button
              onClick={() => setConfirmed(1)}
              className={`py-2 px-4 text-sm  ${
                confirmed === 1
                  ? "bg-green-600 text-white"
                  : "bg-slate-300 hover:bg-slate-200"
              }  rounded-full md:mr-4`}
            >
              Confirmed Diaspora
            </button>
            <button
              onClick={() => setConfirmed(0)}
              className={`py-2 px-4 text-sm ${
                confirmed === 0
                  ? "bg-green-600 text-white"
                  : "bg-slate-300 hover:bg-slate-200"
              }  rounded-full md:mr-4`}
            >
              Non Confirmed Diaspora
            </button>
          </div>
        </div>

        {isLoading && (
          <div className="flex justify-center items-center mt-48">
            <div className="fixed left-2/4">
              <Image src="/img/loading.gif" width="50" height="50" />
            </div>
          </div>
        )}

        {isLoading === false && diaspora && diaspora.length < 1 && (
          <div className="flex justify-center items-center mt-48">
            <div className="fixed left-2/4">
              <p>There is no diaspora</p>
            </div>
          </div>
        )}

        {isLoading === false && (
          <React.Fragment>
            {diaspora &&
              diaspora.map((entry) => (
                <div
                  key={entry?.users_id}
                  className="grid grid-cols-6 p-4 bg-white shadow-md justify-between items-center rounded-md mb-5  hover:bg-slate-200 hover:cursor-pointer"
                >
                  <div className="flex justify-start col-span-full  xl:col-span-1">
                    <div>
                      <Image
                        src={entry?.users_avatar || "/img/avatar_default.jpg"}
                        width="40"
                        height="40"
                        className="rounded-full border-2 border-slate-500"
                      />
                    </div>
                    <div className="ml-6">
                      <p className="text-lg">{entry?.users_name}</p>
                      {confirmed === 1 && (
                        <span className="text-[10px] border-2 rounded-md border-green-600 text-green-600 px-2 font-semibold col-span-full">
                          Confirmed
                        </span>
                      )}

                      {confirmed === 0 && (
                        <span className="text-[10px] border-2 rounded-md bg-red-200 border-red-600 text-red-600 px-2 font-semibold col-span-full">
                          Not Confirmed
                        </span>
                      )}
                    </div>
                  </div>

                  <div className="col-span-full xl:col-span-2">
                    <div className="grid grid-cols-3">
                      <div className="xl:ml-5">
                        <p className="text-lg">From</p>
                        <p className="uppercase text-sm text-slate-500">
                          {getCountryName(
                            entry?.profile_birthCountry ||
                              entry?.profile_residenceContry
                          )}
                        </p>
                      </div>

                      <div className="xl:ml-5">
                        <p className="text-lg">Lives In</p>
                        <p className="uppercase text-sm text-slate-500">
                          {getCountryName(entry?.profile_residenceContry)}
                        </p>
                      </div>

                      <div className="xl:ml-5 mb-4 xl:mb-0">
                        <p className="text-lg">Join date</p>
                        <p className="uppercase text-sm text-slate-500">
                          <Moment format="DD/MM/YYYY">
                            {entry?.users_created_at}
                          </Moment>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="mb-4 xl:mb-0 xl:ml-24 flex items-center justify-between col-span-full xl:col-span-2">
                    {entry?.profile_preferedLanguage === "" ? (
                      <div>
                        <p className="text-lg">Language</p>
                        <span>-</span>
                      </div>
                    ) : (
                      <div className="flex flex-col justify-start">
                        <p className="text-lg mt-3">Language</p>
                        <span className="px-4 py-1 mt-2 text-sm rounded-md bg-[#D1ECF1] text-[#125E7F]">
                          {getLanguageName(entry?.profile_preferedLanguage)}
                        </span>
                      </div>
                    )}

                    <div className="mr-5 xl:mr-12">
                      <Image
                        src="/img/Icon awesome-eye.png"
                        width="30"
                        height="20"
                        className="cursor-pointer"
                        onClick={() => {
                          const payload = {
                            id: entry?.users_id,
                            name: entry?.users_name,
                          };
                          dispatch(fetchSingleRecord(payload));
                          dispatch(open("ViewDiaspora"));
                        }}
                      />
                    </div>
                  </div>

                  <div className="flex items-center justify-between col-span-full xl:col-span-1">
                    <div className="flex flex-col">
                      {confirmed === 0 && (
                        <span
                          onClick={() =>
                            dispatch(confirmDiaspora(entry?.users_id))
                          }
                          className="px-5 py-2 text-sm rounded-md mb-4 bg-green-200 text-green-500 font-semibold hover:bg-green-300 cursor-pointer"
                        >
                          Confirm
                        </span>
                      )}

                      <span
                        onClick={() => {
                          const payload = {
                            id: entry?.users_id,
                            email: entry?.users_email,
                            title: entry?.users_name,
                            lang: entry?.profile_preferedLanguage,
                            email: entry?.users_email,
                          };
                          dispatch(add(payload));
                          dispatch(open("RejectDiaspora"));
                        }}
                        className="px-4 py-2 text-sm rounded-md bg-red-200 text-red-500 font-semibold hover:bg-red-300 cursor-pointer"
                      >
                        Reject
                      </span>
                    </div>
                    <div className="ml-5">
                      <span
                        onClick={() => {
                          const payload = {
                            id: entry?.users_id,
                          };
                          dispatch(add(payload));
                          dispatch(open("ConfirmDeletionDaispora"));
                        }}
                        className="px-4 py-2 text-sm rounded-md bg-red-500 text-white hover:bg-red-600 cursor-pointer"
                      >
                        Delete
                      </span>
                    </div>
                  </div>
                </div>
              ))}
          </React.Fragment>
        )}

        {isLoading === false && diaspora && diaspora.length >= 20 && (
          <ConfirmDiasporaLoadMoreBtn type={confirmed} />
        )}

        {/* Modals */}
        <Modal
          isOpen={modalIsOpen && modalName === "ViewDiaspora"}
          onAfterOpen={() => {
            document.body.style.overflow = "hidden";
          }}
          style={{
            content: {
              height: "auto",
              border: "none",
              background: "transparent !important",
              inset: "0px",
            },
          }}
          contentLabel="View Diaspora"
        >
          <ViewDiasPora />
        </Modal>

        <Modal
          isOpen={modalIsOpen && modalName === "RejectDiaspora"}
          onAfterOpen={() => {
            document.body.style.overflow = "hidden";
          }}
          style={{
            content: {
              height: "auto",
              maxWidth: "800px",
              margin: "0 auto",
              border: "none",
              background: "transparent !important",
              inset: "0px",
            },
          }}
          contentLabel="Reject Diaspora"
        >
          <RejectDiaspora />
        </Modal>

        <Modal
          isOpen={modalIsOpen && modalName === "ConfirmDeletionDaispora"}
          style={{
            content: {
              height: "auto",
              maxWidth: "800px",
              margin: "0 auto",
              border: "none",
              background: "transparent !important",
              inset: "0px",
            },
          }}
          contentLabel="ConfirmDeletionDaispora"
        >
          <ConfirmDeletionDaispora />
        </Modal>
      </Layout>
    </>
  );
};

export const getServerSideProps = withSession(async (ctx) => {
  if (ctx.req.session.get("user") === undefined) {
    ctx.res.setHeader("location", "/login");
    ctx.res.statusCode = 302;
    ctx.res.end();
    return { props: {} };
  }

  const user = ctx.req.session.get("user");

  return {
    props: {
      user,
    },
  };
});

export default ConfirmedDiaspora;

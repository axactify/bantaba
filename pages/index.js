export default function Home() {
  return;
}

export async function getServerSideProps({ res }) {
  res.setHeader("location", "/login");
  res.statusCode = 302;
  res.end();
 
  return {
    props: {},
  };
}

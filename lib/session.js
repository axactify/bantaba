// this file is a wrapper with defaults to be used in both API routes and `getServerSideProps` functions
import { withIronSession } from "next-iron-session";
import config from "./config";

export default function withSession(handler) {
  return withIronSession(handler, config);
}

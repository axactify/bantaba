import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: "",
  name: "",
  open: false,
};

export const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    open: (state, action) => {
      state.name = action.payload;
      state.open = true;
    },
    add: (state, action) => {
      state.data = action.payload;
    },
    close: (state, action) => {
      state.name = action.payload;
      state.open = false;
      document.body.style.overflow = "unset";
    },
  },
});

// Destructure and export the plain action creators
export const { open, add, close } = modalSlice.actions;

export default modalSlice.reducer;

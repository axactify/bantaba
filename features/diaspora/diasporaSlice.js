import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import diasporaService from "./diasporaService";

export const fetchRecords = createAsyncThunk(
  "diaspora/fetchRecords",
  async (status, thunkAPI) => {
    try {
      return await diasporaService.fetchRecords(status);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const fetchSingleRecord = createAsyncThunk(
  "startups/fetchSingle",
  async (id, thunkAPI) => {
    try {
      return await diasporaService.fetchSingleRecord(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const deleteRecord = createAsyncThunk(
  "diaspora/delete",
  async (id, thunkAPI) => {
    try {
      return await diasporaService.deleteRecord(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const loadMoreRecords = createAsyncThunk(
  "startups/loadmore",
  async (page, thunkAPI) => {
    try {
      return await diasporaService.loadMoreRecords(page);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const fetchLastMonthRecords = createAsyncThunk(
  "startups/last-month",
  async (page, thunkAPI) => {
    try {
      return await diasporaService.fetchLastMonthRecords(page);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const rejectDiaspora = createAsyncThunk(
  "startups/reject",
  async (payload, thunkAPI) => {
    try {
      return await diasporaService.RejectDiaspora(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const fetchDiasporaDateRange = createAsyncThunk(
  "diaspora/fetchDiasporaDateRange",
  async (payload, thunkAPI) => {
    try {
      return await diasporaService.fetchDiasporaDateRange(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const confirmDiaspora = createAsyncThunk(
  "startups/confirm-startup",
  async (id, thunkAPI) => {
    try {
      return await diasporaService.confirmDiaspora(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const initialState = {
  diaspora: [],
  selectedDiaspora: "",
  loading: "idle",
  isError: false,
  message: "",
  visibleLoadMoreBtn: true,
  currentPage: 0,
  filterCurrentPage: 0,
  applyFilter: false,
  actionFilter: "",
  startDate: "",
  endDate: "",
};

const diasporaSlice = createSlice({
  name: "diaspora",
  initialState,
  reducers: {
    removeError: (state, action) => {
      state.isError = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchRecords.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(fetchRecords.fulfilled, (state, action) => {
        const { statusMsg, count } = action.payload;
        state.diaspora = action.payload.diaspora;

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          state.loading = false;
        } else {
          state.visibleLoadMoreBtn = true;
          state.applyFilter = false;
          state.loading = false;
        }
      })
      .addCase(fetchRecords.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
        state.diaspora = null;
      });

    builder
      .addCase(fetchSingleRecord.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(fetchSingleRecord.fulfilled, (state, action) => {
        state.loading = false;
        state.selectedDiaspora = action.payload;
      })
      .addCase(fetchSingleRecord.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.selectedDiaspora = null;
        state.message = action.payload;
      });

    builder
      // .addCase(loadMoreRecords.pending, (state, action) => {
      //   state.loading = true;
      // })
      .addCase(loadMoreRecords.fulfilled, (state, action) => {
        const { statusMsg, count, diaspora } = action.payload;

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          // state.loading = false;
        } else {
          state.diaspora = state.diaspora.concat(diaspora);
          state.currentPage += 1;
          state.visibleLoadMoreBtn = true;
          // state.loading = false;
        }
      })
      .addCase(loadMoreRecords.rejected, (state, action) => {
        // state.loading = false;
        state.isError = true;
        state.message = action.payload;
      });

    builder
      // .addCase(fetchLastMonthRecords.pending, (state, action) => {
      //   state.loading = true;
      // })
      .addCase(fetchLastMonthRecords.fulfilled, (state, action) => {
        const { statusMsg, count, diaspora } = action.payload;

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          state.loading = false;
        } else {
          state.actionFilter = "fetchLastMonthRecords";
          state.diaspora = diaspora;
          state.applyFilter = true;
          state.filterCurrentPage += 1;
          state.visibleLoadMoreBtn = true;
          state.loading = false;
        }
      })
      .addCase(fetchLastMonthRecords.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
        state.diaspora = null;
        state.applyFilter = false;
      });

    builder.addCase(deleteRecord.fulfilled, (state, action) => {
      state.diaspora = state.diaspora.filter(
        (x) => x.users_id !== action.payload.id
      );
    });

    builder.addCase(rejectDiaspora.rejected, (state, action) => {
      state.isError = true;
      state.message = action.payload;
    });

    builder
      // .addCase(fetchDiasporaDateRange.pending, (state, action) => {
      //   state.loading = true;
      // })
      .addCase(fetchDiasporaDateRange.fulfilled, (state, action) => {
        const { statusMsg, message, count, diaspora, startDate, endDate } =
          action.payload;

        state.diaspora = diaspora;

        if (statusMsg === "Error") {
          state.loading = false;
          state.message = message;
          state.isError = true;
          state.diaspora = [];
          state.applyFilter = false;
          state.actionFilter = "";
        }

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          state.loading = false;
        } else {
          state.applyFilter = true;
          state.actionFilter = "fetchDiasporaDateRange";
          state.filterCurrentPage += 1;
          state.visibleLoadMoreBtn = true;
          state.loading = false;
        }
      })
      .addCase(fetchDiasporaDateRange.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
        state.diaspora = null;
        state.applyFilter = false;
        state.actionFilter = "";
      });

    builder.addCase(confirmDiaspora.fulfilled, (state, action) => {
      state.diaspora = state.diaspora.filter(
        (x) => x.users_id !== action.payload.id
      );
    });
  },
});

export const { removeError } = diasporaSlice.actions;

export default diasporaSlice.reducer;

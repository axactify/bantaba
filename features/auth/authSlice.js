import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import authService from "./authService";

export const resetPassword = createAsyncThunk(
  "auth/resetpassword",
  async (payload, thunkAPI) => {
    try {
      return await authService.resetPassword(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const loginUser = createAsyncThunk(
  "auth/loginuser",
  async (payload, thunkAPI) => {
    try {
      return await authService.login(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const forgotPassword = createAsyncThunk(
  "auth/forgot",
  async (payload, thunkAPI) => {
    try {
      return await authService.forgotPassword(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const initialState = {
  isLogin: false,
  isError: false,
  message: "",
  isFound: false,
  isProcessing: false,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    removeError: (state, action) => {
      state.isError = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginUser.pending, (state, action) => {
        state.isProcessing = true;
      })
      .addCase(loginUser.fulfilled, (state, action) => {
        if (action.payload.statusCode === 401) {
          state.isError = true;
          state.message = action.payload;
          state.isProcessing = false;
        }
        state.isLogin = true;
      })
      .addCase(loginUser.rejected, (state, action) => {
        state.isError = true;
        state.message = action.payload;
        state.isProcessing = false;
        alert(action.payload);
      });

    builder
      .addCase(resetPassword.fulfilled, (state, action) => {
        state.isFound = false;
      })
      .addCase(resetPassword.rejected, (state, action) => {
        state.isError = true;
        state.message = action.payload;
        alert(action.payload);
      });

    builder
      .addCase(forgotPassword.pending, (state, action) => {
        state.isProcessing = true;
      })
      .addCase(forgotPassword.fulfilled, (state, action) => {
        state.isFound = true;
        state.isProcessing = false;
      })
      .addCase(forgotPassword.rejected, (state, action) => {
        state.isError = true;
        state.message = action.payload;
        state.isFound = false;
        state.isProcessing = false;
        alert(action.payload);
      });
  },
});

export const { removeError } = authSlice.actions;
export default authSlice.reducer;

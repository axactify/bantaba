import axios from "axios";

export const login = async (payload) => {
  const url = `/api/login`;
  const response = await axios.post(url, payload);
  return response.data;
};

export const resetPassword = async (payload) => {
  const url = `https://dashapi-dev.ourbantaba.com/users/reset/gc9540b626`;
  const response = await axios.post(url, payload);
  return response.data;
};

export const forgotPassword = async (payload) => {
  const url = `https://dashapi-dev.ourbantaba.com/users/forgot`;
  const response = await axios.post(url, payload);
  return response.data;
};

const authService = {
  login,
  forgotPassword,
};

export default authService;

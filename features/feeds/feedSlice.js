import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import feedService from "./feedService";

export const fetchFeeds = createAsyncThunk(
  "feeds/fetcRecords",
  async (language, thunkAPI) => {
    try {
      return await feedService.fetchFeeds(language);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const loadMoreRecords = createAsyncThunk(
  "feeds/loadmore",
  async (page, thunkAPI) => {
    try {
      return await feedService.loadMoreRecords(page);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const deleteFeed = createAsyncThunk(
  "feeds/delete",
  async (id, thunkAPI) => {
    try {
      return await feedService.deleteFeed(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const rejectFeed = createAsyncThunk(
  "feeds/reject",
  async (id, thunkAPI) => {
    try {
      return await feedService.rejectFeed(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const approveFeed = createAsyncThunk(
  "feeds/approve",
  async (id, thunkAPI) => {
    try {
      return await feedService.approveFeed(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const initialState = {
  feeds: [],
  isError: false,
  message: "",
  loading: "idle",
  visibleLoadMoreBtn: true,
  currentPage: 0,
  isError: false,
  message: "",
};

const feedSlice = createSlice({
  name: "feed",
  initialState,
  reducers: {
    removeError: (state, action) => {
      state.isError = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchFeeds.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(fetchFeeds.fulfilled, (state, action) => {
        console.log(action.payload.feeds);
        state.feeds = action.payload.feeds;
        state.loading = false;
        state.visibleLoadMoreBtn = true;
      })
      .addCase(fetchFeeds.rejected, (state, action) => {
        state.isError = true;
        state.loading = false;
        state.message = action.payload;
      });

    builder
      // .addCase(loadMoreRecords.pending, (state, action) => {
      //   state.loading = true;
      // })
      .addCase(loadMoreRecords.fulfilled, (state, action) => {
        const { statusMsg, count, feeds } = action.payload;

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          state.loading = false;
        } else {
          state.feeds = state.feeds.concat(feeds);
          state.currentPage += 1;
          state.visibleLoadMoreBtn = true;
          state.loading = false;
        }
      })
      .addCase(loadMoreRecords.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
      });

    builder
      .addCase(deleteFeed.fulfilled, (state, action) => {
        state.feeds = state.feeds.filter(
          (feed) => feed.id !== action.payload.id
        );
        alert("Feed delete successfull");
      })
      .addCase(deleteFeed.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
      });

    builder
      .addCase(approveFeed.fulfilled, (state, action) => {
        state.feeds = state.feeds.filter(
          (feed) => feed.id !== action.payload.id
        );
        alert("Feed approved successfull");
      })
      .addCase(approveFeed.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
      });

    builder
      .addCase(rejectFeed.fulfilled, (state, action) => {
        state.feeds = state.feeds.filter(
          (feed) => feed.id !== action.payload.id
        );
        alert("Feed rejected successfull");
      })
      .addCase(rejectFeed.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});

export const { removeError } = feedSlice.actions;

export default feedSlice.reducer;

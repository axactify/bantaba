import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import startupService from "./startupService";

export const fetchStartups = createAsyncThunk(
  "startups/fetchRecords",
  async (type, thunkAPI) => {
    try {
      return await startupService.fetchRecords(type);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const fetchSingleRecord = createAsyncThunk(
  "startups/fetchSingle",
  async (payload, thunkAPI) => {
    try {
      return await startupService.fetchSingleRecord(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const deleteRecord = createAsyncThunk(
  "startups/delete",
  async (id, thunkAPI) => {
    try {
      return await startupService.deleteRecord(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const loadMoreRecords = createAsyncThunk(
  "startups/loadmore",
  async (payload, thunkAPI) => {
    try {
      return await startupService.loadMoreRecords(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const fetchLastMonthRecords = createAsyncThunk(
  "startups/last-month",
  async (page, thunkAPI) => {
    try {
      return await startupService.fetchLastMonthRecords(page);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const rejectStartup = createAsyncThunk(
  "startups/reject",
  async (payload, thunkAPI) => {
    try {
      return await startupService.rejectStartup(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const fetchStartupDateRange = createAsyncThunk(
  "startups/fetchStartupDateRange",
  async (payload, thunkAPI) => {
    try {
      return await startupService.fetchStartupDateRange(payload);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const confirmStartup = createAsyncThunk(
  "startups/confirm-startup",
  async (id, thunkAPI) => {
    try {
      return await startupService.confirmStartup(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const initialState = {
  startups: [],
  selectedStartup: "",
  loading: "idle",
  isError: false,
  message: "",
  visibleLoadMoreBtn: true,
  currentPage: 0,
  filterCurrentPage: 0,
  applyFilter: false,
  actionFilter: "",
  startDate: "",
  endDate: "",
};

const startupSlice = createSlice({
  name: "startup",
  initialState,
  reducers: {
    removeError: (state, action) => {
      state.isError = false;
    },
    resetFilter: (state, action) => {
      state.applyFilter = false;
      state.endDate = "";
      state.startDate = "";
      state.currentPage = 0;
      state.filterCurrentPage = 0;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchStartups.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(fetchStartups.fulfilled, (state, action) => {
        const { statusMsg, count } = action.payload;

        state.startups = action.payload.startups;

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          state.loading = false;
        } else {
          state.visibleLoadMoreBtn = true;
          state.loading = false;
        }
      })
      .addCase(fetchStartups.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.startups = null;
        state.message = action.payload;
        state.visibleLoadMoreBtn = false;
      });

    builder
      .addCase(fetchSingleRecord.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(fetchSingleRecord.fulfilled, (state, action) => {
        const { statusMsg, count } = action.payload;

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          state.loading = false;
        } else {
          state.selectedStartup = action.payload;
          state.loading = false;
        }
      })
      .addCase(fetchSingleRecord.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
        state.selectedStartup = null;
      });

    builder.addCase(deleteRecord.fulfilled, (state, action) => {
      state.startups = state.startups.filter(
        (startup) => startup.startups_id !== action.payload.startup_id
      );
      alert("Startup delete successfull");
    });

    builder
      // .addCase(loadMoreRecords.pending, (state, action) => {
      //   state.loading = true;
      // })
      .addCase(loadMoreRecords.fulfilled, (state, action) => {
        const { statusMsg, count, startups } = action.payload;

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          // state.loading = false;
        } else {
          state.startups = state.startups.concat(startups);
          state.currentPage += 1;
          state.visibleLoadMoreBtn = true;
          // state.loading = false;
        }
      })
      .addCase(loadMoreRecords.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
      });

    builder
      // .addCase(fetchLastMonthRecords.pending, (state, action) => {
      //   state.loading = true;
      // })
      .addCase(fetchLastMonthRecords.fulfilled, (state, action) => {
        const { statusMsg, count, startups } = action.payload;

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          state.loading = false;
        } else {
          state.applyFilter = true;
          state.actionFilter = "fetchLastMonthRecords";
          state.filterCurrentPage += 1;
          state.startups = startups;
          state.visibleLoadMoreBtn = true;
          state.loading = false;
        }
      })
      .addCase(fetchLastMonthRecords.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
        state.startups = null;
        state.applyFilter = false;
        state.actionFilter = "";
      });

    builder
      // .addCase(fetchStartupDateRange.pending, (state, action) => {
      //   state.loading = true;
      // })
      .addCase(fetchStartupDateRange.fulfilled, (state, action) => {
        const { statusMsg, message, count, startups, startDate, endDate } =
          action.payload;

        state.startups = startups;

        if (statusMsg === "Error") {
          state.loading = false;
          state.message = message;
          state.isError = true;
          state.startups = [];
          state.applyFilter = false;
          state.actionFilter = "";
        }

        if (statusMsg === "Success" && count === 0) {
          state.visibleLoadMoreBtn = false;
          state.loading = false;
        } else {
          state.applyFilter = true;
          state.actionFilter = "fetchStartupDateRange";
          state.startDate = startDate;
          state.endDate = endDate;
          state.filterCurrentPage += 1;
          state.visibleLoadMoreBtn = true;
          state.loading = false;
        }
      })
      .addCase(fetchStartupDateRange.rejected, (state, action) => {
        state.loading = false;
        state.isError = true;
        state.message = action.payload;
        state.startups = null;
        state.applyFilter = false;
        state.actionFilter = "";
      });

    builder.addCase(rejectStartup.rejected, (state, action) => {
      state.isError = true;
      state.message = action.payload;
    });

    builder.addCase(confirmStartup.fulfilled, (state, action) => {
      state.startups = state.startups.filter(
        (startup) => startup.startups_id !== action.payload.startup_id
      );
    });
  },
});

export const { removeError, resetFilter } = startupSlice.actions;

export default startupSlice.reducer;
